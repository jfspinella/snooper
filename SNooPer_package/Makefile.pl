use strict;
use warnings;
use ExtUtils::MakeMaker;

WriteMakefile(

    NAME		=> 'SNooPer::Lib_SNooPer',
    VERSION_FROM	=> 'lib/SNooPer/Lib_SNooPer.pm',
    EXE_FILES 		=> [ 'script/SNooPer.pl' ],
    ABSTRACT	  	=> 'SNooPer is a highly versatile data mining approach that uses Leo Breiman s Random Forest classification models to accurately call somatic variants in low-depth sequencing data.',
    AUTHOR         	=> 'JF.Spinella <jf.spinella@gmail.com>',
    PREREQ_PM => {
      'Getopt::Long'       => 0,
      'List::Util'         => 0,
      'File::Path'         => 0,
      'Math::CDF'          => 0,
      'Text::NSP'          => 0,
      'Statistics::R'      => 0,
      'Statistics::Test'   => 0
    },
    
    dist                => { COMPRESS => 'gzip -9f', SUFFIX => 'gz', },
    clean               => { FILES => 'SNooPer-Lib_SNooPer-*' }

);

1;
