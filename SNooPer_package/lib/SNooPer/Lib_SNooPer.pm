#package SNooPer::Lib_SNooPer;
package Lib_SNooPer;
use version; our $VERSION = '0.01';

#======================
#  Load Libraries
#======================
use strict;
use warnings;
use List::Util qw(sum);
use File::Path qw(make_path);
#use Math::CDF qw(:all);
#use Text::NSP::Measures::2D::Fisher::right;
#use Statistics::Test::WilcoxonRankSum;
#use Statistics::R;



#====================================================
#                 Options testing
#====================================================

sub OptionsTest{

  #variables declaration
  my $algorithm = shift;
  my $attributes_selection = shift // 'off';
  my $bqv = shift // 20;
  my $contamination = shift // 0;
  my $cost_matrix = shift;
  my $coverage_filter_N = shift // 8;
  my $coverage_filter_T = shift // 8;
  my $covered_filter_N = shift // 'on';
  my $cross_validation = shift // 10;
  my $freqinf = shift // 0;
  my $freqsup = shift // 1;
  my $indel_filter = shift // 'on';
  my $input_directory = shift;
  my $job_id = shift // 'new';
  my $memory = shift // '-Xmx2g';
  my $mqv = shift // 20;
  my $nbnonvalidated = shift;
  my $nbvalidated = shift;
  my $nbvar_N = shift // 1; 
  my $nbvar_T = shift // 1; 
  my $output_directory = shift;
  my $path_to_bedtool = shift;
  my $path_to_blacklist = shift;
  my $path_to_germDB = shift;
  my $path_to_model_directory = shift;
  my $path_to_weka = shift;
  my $platform1 = shift // '>Illumina-1.8';
  my $qual_filter = shift // 'on';
  my $somatic_pvalue = shift // 0.05;
  my $tree = shift // 300;
  my $type_of_analysis1 = shift;
  my $type_of_analysis2 = shift;
  my $type_of_analysis3 = shift // 'SNP';
  my $validated_nonvalidated_ratio = shift // 0.1;
  my $validated_variant_fraction = shift // 1;
  my ($temp_output_directory,$bqv_platform1,$mqv_platform1,$ascii_correction_p1,$path_to_model,$path_to_attributes,@selected_attributes);
  my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
  my $date = $mday."-".($mon+1)."-".(1900+$year)."_".$hour.":".$min.":".$sec;

  # Options
  #----------------------------------------------------------------------------
  
  # Attributes selection
  if($attributes_selection ne "MI" && $attributes_selection ne "BestFirst" && $attributes_selection ne "off"){
    print "Error, attributes selection has to take one of these values: off, MI or BestFirst, the default value is off\n";
    exit 0;
  }
  
  # Contamination
  if($contamination >= 1){
    print "Error, contamination has to take a value between 0 and 1, the default value is 0\n";
    exit 0;
  }
  
  # Covered filter N
  if($covered_filter_N ne "on" && $covered_filter_N ne "off"){
    print "Error, covered filter N has to take one of these values: on or off, the default value is on\n";
    exit 0;
  }
  
  # Indel filter
  if($indel_filter ne "on" && $indel_filter ne "off"){
    print "Error, indel filter has to take one of these values: on or off, the default value is on\n";
    exit 0;
  }
  
  # Platform definition
  if(defined $platform1){
    if($platform1 ne "Solid" && $platform1 ne "Solexa" && $platform1 ne "Illumina-1.3" && $platform1 ne "Illumina-1.5" && $platform1 ne ">Illumina-1.8"){ 
      print "Error, platform 1 has to take one of these values: Solid, Solexa, Illumina-1.3, Illumina-1.5 or >Illumina-1.8, the default value is >Illumina-1.8\n";
      exit 0;
    }
  }
  if($platform1 eq "Solid" || $platform1 eq ">Illumina-1.8"){
    $ascii_correction_p1 = 33;
    $bqv_platform1 = $bqv+$ascii_correction_p1;
    $mqv_platform1 = $mqv+$ascii_correction_p1;
  }elsif($platform1 eq "Solexa" || $platform1 eq "Illumina-1.3" || $platform1 eq "Illumina-1.5"){
    $ascii_correction_p1 = 64;
    $bqv_platform1 = $bqv+$ascii_correction_p1;
    $mqv_platform1 = $mqv+$ascii_correction_p1;
  }
  
  # Quality filter
  if($qual_filter ne "on" && $qual_filter ne "on+" && $qual_filter ne "off" && $qual_filter ne "off+"){
    print "Error, quality filter has to take one of these values: on, on+, off or off+, the default value is on\n";
    exit 0;
  }
  
  # Somatic p-value
  if($somatic_pvalue > 1){
    print "Error, somatic pvalue has to take a value between 0 and 1, the default value is 0.05\n";
    exit 0;
  }
  
  # Analysis type1 definition
  if($type_of_analysis1 ne "somatic" && $type_of_analysis1 ne "germline"){
    print "Error, the type of analysis 1 has to take one of these values: somatic or germline\n";
    exit 0;
  }
  
  # Analysis type2 definition
  if($type_of_analysis2 ne "train" && $type_of_analysis2 ne "classify" && $type_of_analysis2 ne "evaluate"){
    print "Error, the type of analysis 2 has to take one of these values: train, classify or evaluate\n";
    exit 0;
  }
  
  # Analysis type3 definition
  if($type_of_analysis3 ne "SNP" && $type_of_analysis3 ne "Indel"){
    print "Error, the type of analysis 3 has to take one of these values: SNP or Indel, the default value is SNP\n";
    exit 0;
  }
  
  # Validated / Non-validated ratio
  if($validated_nonvalidated_ratio > 1){
    print "Error, validated / non-validated ratio has to take a value between 0 and 1, the default value is 0.1\n";
    exit 0;
  }
  
  # frequency boundaries
  if($freqinf > 1){
    print "Error, the inferior limit of allele frequency has to take a value between 0 and 1, the default value is 0\n";
    exit 0;
  }
  if($freqsup > 1){
    print "Error, the superior limit of allele frequency has to take a value between 0 and 1, the default value is 1\n";
    exit 0;
  }
  if($freqinf > $freqsup){
    print "Error, the inferior limit of allele frequency can't be greater than the superior limit of allele frequency\n";
    exit 0;
  }
  
  # Contamination
  if($validated_variant_fraction > 1){
    print "Error, validated variant fraction has to take a value between 0 and 1, the default value is 1\n";
    exit 0;
  }
  
  # Path definition
  #----------------------------------------------------------------------------
  
  # Input
  if(! defined $input_directory){
    print "Error, input directory has to be defined\n";
    exit 0;
  }elsif($input_directory !~ /\/$/){
    $input_directory = $input_directory."/";
  }
  if($type_of_analysis2 eq "evaluate"){
    print "Info: To evaluate the model with a chosen sample (not used to train the model), the input files should be located in a fresh directory containing these files only\n-----\n";
  }
  
  # Output
  if(! defined $output_directory){
    print "Error, output directory has to be defined\n";
    exit 0;
  }elsif($output_directory =~ /\/$/){
    make_path($output_directory."SNooPer_output_".$job_id."_".$date);
    $output_directory = $output_directory."SNooPer_output_".$job_id."_".$date."/";
  }else{
    make_path($output_directory."/SNooPer_output_".$job_id."_".$date);
    $output_directory = $output_directory."/SNooPer_output_".$job_id."_".$date."/";
  }
  make_path($output_directory."temp");
  $temp_output_directory = $output_directory."temp/";
  
  # Model
  if($type_of_analysis2 eq "train"){
  
    make_path($output_directory."model");
    $path_to_model_directory = $output_directory."model/";
    $path_to_attributes = $path_to_model_directory.$algorithm.'_trained_model_attributes.txt';
    
    if($type_of_analysis3 eq "SNP"){
      if($qual_filter eq "on"){
	@selected_attributes = ('highqualcov_vs_med','LocMean_vs_med','LocMean_vs_ref','indelflag','FRratioqual','SBpvalqual','SBpbinomqual','var_mean_bqv_quality','bqv_ratio_vs_med','var_mean_mqv_quality','mqv_ratio_vs_med','QBpval','GQBprob','RQBprob','allelic_freq_highqual');
	if($attributes_selection eq "off"){
	  open (ATT_OUTPUT, ">$path_to_attributes") or die "Cannot open file $path_to_attributes: $!";
	  print (ATT_OUTPUT "Selected attributes: 2,3,4,5,9,10,11,12,13,14,15,16,17,18,20 : 15\nQuality filter: $qual_filter\nAttributes selection: $attributes_selection");
	  close ATT_OUTPUT;
	}
      }elsif($qual_filter eq "on+" || $qual_filter eq "off+"){
	@selected_attributes = ('coverage_vs_med','highqualcov_vs_med','LocMean_vs_med','LocMean_vs_ref','indelflag','FRratio','SBpval','SBpbinom','FRratioqual','SBpvalqual','SBpbinomqual','var_mean_bqv_quality','bqv_ratio_vs_med','var_mean_mqv_quality','mqv_ratio_vs_med','QBpval','GQBprob','RQBprob','allelic_freq_raw','allelic_freq_highqual');
	if($attributes_selection eq "off"){
	  open (ATT_OUTPUT, ">$path_to_attributes") or die "Cannot open file $path_to_attributes: $!";
	  print (ATT_OUTPUT "Selected attributes: 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20 : 20\nQuality filter: $qual_filter\nAttributes selection: $attributes_selection");
	  close ATT_OUTPUT;
	}
      }elsif($qual_filter eq "off"){
	@selected_attributes = ('coverage_vs_med','LocMean_vs_med','LocMean_vs_ref','indelflag','FRratio','SBpval','SBpbinom','var_mean_bqv_quality','bqv_ratio_vs_med','var_mean_mqv_quality','mqv_ratio_vs_med','QBpval','GQBprob','RQBprob','allelic_freq_raw');
	if($attributes_selection eq "off"){
	  open (ATT_OUTPUT, ">$path_to_attributes") or die "Cannot open file $path_to_attributes: $!";
	  print (ATT_OUTPUT "Selected attributes: 1,3,4,5,6,7,8,12,13,14,15,16,17,18,19 : 15\nQuality filter: $qual_filter\nAttributes selection: $attributes_selection");
	  close ATT_OUTPUT;
	}
      }
    }else{
      if($qual_filter eq "on"){
	@selected_attributes = ('highqualcov_vs_med','LocMean_vs_med','LocMean_vs_ref','FRratioqual','SBpvalqual','SBpbinomqual','var_mean_bqv_quality','bqv_ratio_vs_med','var_mean_mqv_quality','mqv_ratio_vs_med','QBpval','GQBprob','RQBprob','allelic_freq_highqual');
	if($attributes_selection eq "off"){
	  open (ATT_OUTPUT, ">$path_to_attributes") or die "Cannot open file $path_to_attributes: $!";
	  print (ATT_OUTPUT "Selected attributes: 2,3,4,9,10,11,12,13,14,15,16,17,18,20 : 14\nQuality filter: $qual_filter\nAttributes selection: $attributes_selection");
	  close ATT_OUTPUT;
	}
      }elsif($qual_filter eq "on+" || $qual_filter eq "off+"){
	@selected_attributes = ('coverage_vs_med','highqualcov_vs_med','LocMean_vs_med','LocMean_vs_ref','FRratio','SBpval','SBpbinom','FRratioqual','SBpvalqual','SBpbinomqual','var_mean_bqv_quality','bqv_ratio_vs_med','var_mean_mqv_quality','mqv_ratio_vs_med','QBpval','GQBprob','RQBprob','allelic_freq_raw','allelic_freq_highqual');
	if($attributes_selection eq "off"){
	  open (ATT_OUTPUT, ">$path_to_attributes") or die "Cannot open file $path_to_attributes: $!";
	  print (ATT_OUTPUT "Selected attributes: 1,2,3,4,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20 : 19\nQuality filter: $qual_filter\nAttributes selection: $attributes_selection");
	  close ATT_OUTPUT;
	}
      }elsif($qual_filter eq "off"){
	@selected_attributes = ('coverage_vs_med','LocMean_vs_med','LocMean_vs_ref','FRratio','SBpval','SBpbinom','var_mean_bqv_quality','bqv_ratio_vs_med','var_mean_mqv_quality','mqv_ratio_vs_med','QBpval','GQBprob','RQBprob','allelic_freq_raw');
	if($attributes_selection eq "off"){
	  open (ATT_OUTPUT, ">$path_to_attributes") or die "Cannot open file $path_to_attributes: $!";
	  print (ATT_OUTPUT "Selected attributes: 1,3,4,6,7,8,12,13,14,15,16,17,18,19 : 14\nQuality filter: $qual_filter\nAttributes selection: $attributes_selection");
	  close ATT_OUTPUT;
	}
      }
    }
   
  }elsif($type_of_analysis2 eq "classify" || $type_of_analysis2 eq "evaluate"){
  
    if(! defined $path_to_model_directory){
      print "Error, the path to model directory has to be defined\n";
      exit 0;
    }else{
      $path_to_model = $path_to_model_directory.$algorithm.'_trained.model';
      $path_to_attributes = $path_to_model_directory.$algorithm.'_trained_model_attributes.txt';
      my @available_attributes; my @model_attributes;
      
      open (ATT_INPUT, "<$path_to_attributes") or die "Cannot open file $path_to_attributes, you need to provide the path to model directory: $!";
      while (my $l = <ATT_INPUT>){
	if($l =~ /Selected attributes: (.+) :/){
	  @model_attributes = split(',', $1);
	}
	if($l =~ /Quality filter: (.+)/){
	  $qual_filter = $1;
	}
	if($l =~ /Attributes selection: (.+)/){
	  $attributes_selection = $1;
	}
      }
      close ATT_INPUT;
      
      if($type_of_analysis3 eq "SNP"){
	if($attributes_selection eq "off" || $qual_filter eq "on+" || $qual_filter eq "off+"){
	  @available_attributes = ('coverage_vs_med','highqualcov_vs_med','LocMean_vs_med','LocMean_vs_ref','indelflag','FRratio','SBpval','SBpbinom','FRratioqual','SBpvalqual','SBpbinomqual','var_mean_bqv_quality','bqv_ratio_vs_med','var_mean_mqv_quality','mqv_ratio_vs_med','QBpval','GQBprob','RQBprob','allelic_freq_raw','allelic_freq_highqual');
	}elsif($qual_filter eq "on"){
	  @available_attributes = ('highqualcov_vs_med','LocMean_vs_med','LocMean_vs_ref','indelflag','FRratioqual','SBpvalqual','SBpbinomqual','var_mean_bqv_quality','bqv_ratio_vs_med','var_mean_mqv_quality','mqv_ratio_vs_med','QBpval','GQBprob','RQBprob','allelic_freq_highqual');
	}elsif($qual_filter eq "off"){
	  @available_attributes = ('coverage_vs_med','LocMean_vs_med','LocMean_vs_ref','indelflag','FRratio','SBpval','SBpbinom','var_mean_bqv_quality','bqv_ratio_vs_med','var_mean_mqv_quality','mqv_ratio_vs_med','QBpval','GQBprob','RQBprob','allelic_freq_raw');
	}
      }else{
	if($attributes_selection eq "off" || $qual_filter eq "on+" || $qual_filter eq "off+"){
	  @available_attributes = ('coverage_vs_med','highqualcov_vs_med','LocMean_vs_med','LocMean_vs_ref','FRratio','SBpval','SBpbinom','FRratioqual','SBpvalqual','SBpbinomqual','var_mean_bqv_quality','bqv_ratio_vs_med','var_mean_mqv_quality','mqv_ratio_vs_med','QBpval','GQBprob','RQBprob','allelic_freq_raw','allelic_freq_highqual');
	}elsif($qual_filter eq "on"){
	  @available_attributes = ('highqualcov_vs_med','LocMean_vs_med','LocMean_vs_ref','FRratioqual','SBpvalqual','SBpbinomqual','var_mean_bqv_quality','bqv_ratio_vs_med','var_mean_mqv_quality','mqv_ratio_vs_med','QBpval','GQBprob','RQBprob','allelic_freq_highqual');
	}elsif($qual_filter eq "off"){
	  @available_attributes = ('coverage_vs_med','LocMean_vs_med','LocMean_vs_ref','FRratio','SBpval','SBpbinom','var_mean_bqv_quality','bqv_ratio_vs_med','var_mean_mqv_quality','mqv_ratio_vs_med','QBpval','GQBprob','RQBprob','allelic_freq_raw');
	}
      }
      
      foreach my $i(@model_attributes){
	push(@selected_attributes, $available_attributes[$i-1]);
      }
    }
    
  }
  
  # Path to weka
  if(! defined $path_to_weka){
    print "Error, the path to weka software has to be defined\n";
    exit 0;
  }elsif($path_to_weka =~ /\/$/){
    $path_to_weka = $path_to_weka."weka.jar";
  }
  
  # Path to bedtools
  if(defined $path_to_blacklist && ! defined $path_to_bedtool){
    print "Error, the path to bedtools has to be defined\n";
    exit 0;
  }elsif(defined $path_to_germDB && ! defined $path_to_bedtool){
    print "Error, the path to bedtools has to be defined\n";
    exit 0;
  }elsif($path_to_bedtool =~ /\/$/){
    $path_to_bedtool = $path_to_bedtool."bedtools";
  }
  
  # Path to BlackList track
  if(defined $path_to_blacklist && $path_to_blacklist !~ /.bed$/){
    print "Error, the path to BlackList has to contain the file name and this file has to be in a .bed format\n";
    exit 0;
  }
  
  # Path to germDB track 
  if($type_of_analysis1 eq "somatic" && defined $path_to_germDB && $path_to_germDB !~ /.bed$/){
    print "Error, the path to germDB has to contain the file name and this file has to be in a .bed format\n";
    exit 0;
  }
  
  # Options analysis type-specific
  #----------------------------------------------------------------------------
  
  # Cost matrix
  if($type_of_analysis2 eq "train"){
    if(defined $cost_matrix){
      if($cost_matrix =~ /\[\d*\.\d* \d*\.\d*; \d*\.\d* \d*\.\d*\]/){
	print "Cost matrix well defined\n";
      }else{
	print "
The cost matrix has to be define in a single line format ex: [0.0 1.0; 1.0 0.0]
								   ^    ^  
							  cost on FP    cost on FN\n";
      exit 0;
      }
    }  
    
    # Number of validated variant to consider
    if(! defined $nbvalidated){
      print "Number of validated variants to consider for training hasn't been defined, the pre-defined ratio will be applied\n";
      $nbvalidated = 0;
    }
    
    # Number of non-validated variant to consider
    if(! defined $nbnonvalidated){
      print "Number of non-validated variants to consider for training hasn't been defined, the pre-defined ratio will be applied\n";
      $nbnonvalidated = 0;
    }
  
  }
 
  return($ascii_correction_p1, $bqv_platform1, $contamination, $coverage_filter_N, $coverage_filter_T, $covered_filter_N, $cross_validation, $freqinf, $freqsup, $indel_filter, $input_directory, $memory, $mqv_platform1, $nbnonvalidated, $nbvalidated, $nbvar_N, $nbvar_T, $output_directory, $path_to_bedtool, $path_to_model_directory, $path_to_weka, $qual_filter, $somatic_pvalue, $temp_output_directory, $tree, $type_of_analysis1, $type_of_analysis3, $validated_nonvalidated_ratio, $validated_variant_fraction, \@selected_attributes);
}



#====================================================
#      Directory reading and sample IDs saving
#====================================================

sub DirRead{
  
  #variables declaration
  my $input_directory = shift;
  my $type_of_analysis1 = shift;
  my $type_of_analysis2 = shift;
  
  if($type_of_analysis1 eq "somatic"){
    my (@sample_t1, @sample_n1, @sample_t2);
    
    opendir(DIR, $input_directory) or die "cannot open directory $input_directory";
    my @files = readdir DIR;
    foreach (@files){
      if($_ =~ /Platform1_T_(\w+).pu/){push(@sample_t1, $1);}
      elsif($_ =~ /Platform1_N_(\w+).pu/){push(@sample_n1, $1);}
      elsif($_ =~ /Platform2_T_(\w+).vcf/){push(@sample_t2, $1);}
    }
    
    if($type_of_analysis2 eq "train" || $type_of_analysis2 eq "evaluate"){
	if(scalar(@sample_t1) == 0){
	  print "Error, no file with the appropriate format (Platform1_T_sampleID.pu) has been seen in the input directory\n";
	  exit 0;
	}
	if(scalar(@sample_t2) == 0){
	  print "Error, no file with the appropriate format (Platform2_T_sampleID.vcf) has been seen in the input directory\n";
	  exit 0;
	}
	if(scalar(@sample_n1) == 0){
	  print "Error, no file with the appropriate format (Platform1_N_sampleID.pu) has been seen in the input directory\n";
	  exit 0;
	}
    }elsif($type_of_analysis2 eq "classify"){
	if(scalar(@sample_t1) == 0){
	  print "Error, no file with the appropriate format (Platform1_T_sampleID.pu) has been seen in the input directory\n";
	  exit 0;
	}
	if(scalar(@sample_n1) == 0){
	  print "Error, no file with the appropriate format (Platform1_N_sampleID.pu) has been seen in the input directory\n";
	  exit 0;
	}
    }
    
    close DIR;
    return (\@sample_t1, \@sample_n1, \@sample_t2);
  
  }elsif($type_of_analysis1 eq "germline"){
    my (@sample_1, @sample_2);
    
    opendir(DIR, $input_directory) or die "cannot open directory $input_directory";
    my @files = readdir DIR;
    foreach (@files){
      if($_ =~ /Platform1_(\w+).pu/){push(@sample_1, $1);}
      elsif($_ =~ /Platform2_(\w+).vcf/){push(@sample_2, $1);}
    }
    
    if($type_of_analysis2 eq "train" || $type_of_analysis2 eq "evaluate"){
	if(scalar(@sample_1) == 0){
	  print "Error, no file with the appropriate format (Platform1_sampleID.pu) has been seen in the input directory\n";
	  exit 0;
	}
	if(scalar(@sample_2) == 0){
	  print "Error, no file with the appropriate format (Platform2_sampleID.vcf) has been seen in the input directory\n";
	  exit 0;
	}
    }elsif($type_of_analysis2 eq "classify"){
	if(scalar(@sample_1) == 0){
	  print "Error, no file with the appropriate format (Platform1_sampleID.pu) has been seen in the input directory\n";
	  exit 0;
	}
    }
 
    close DIR;
    return (\@sample_1, \@sample_2);
    
  }
}



#====================================================
# Test intersection between platform1 and platform2
#====================================================

sub InterSectTest{

  #variables declaration
  my($sample_platform1, $sample_platform2) = @_;
  my @sample_platform1_array = @{$sample_platform1};
  my @sample_platform2_array = @{$sample_platform2};
  my %sample= ();
  my @intersect = ();
  
  map{$sample{$_}=1}@sample_platform1_array;
  @intersect = grep{$sample{$_}}@sample_platform2_array;
  if(@intersect == 0){my @refreadquality_val =(); 
    print "Error, missing intersections between input files from platform1 and platform2 or input filename not recognized\n";
    exit 0;
  }
  print "Test intersection between platform1 and platform2 done\n";
  
  return(\@intersect);
}



#====================================================
#      Test match between N and T for somatic
#====================================================

sub SomVsGermTest{
  
  #variables declaration
  my($sample_t1, $sample_n1) = @_;
  my @sample_t1_array = @{$sample_t1};
  my @sample_n1_array = @{$sample_n1};
  my @sorted_sample_t1 = sort @sample_t1_array;
  my @sorted_sample_n1 = sort @sample_n1_array;
   
  if(@sorted_sample_t1 ~~ @sorted_sample_n1){
    print "Test match between N and T for somatic done\n";
  }else{
    print "Error, missing matches between somatic and germline for somatic analysis or input filename not recognized\n";
    exit 0;
  }
  
  return();
}


  
#====================================================
#                  Pileup analysis
#====================================================

sub PileupSNPAnalysis{
  
    #variables declaration
    my $input_directory = shift;
    my $inputFile = shift;
    my $temp_output_directory = shift;
    my $sample_id = shift;
    my $bqv = shift;
    my $mqv = shift;
    my $nbvar_filter = shift;
    my $coverage_filter = shift;
    my $indel_filter = shift;
    my $qual_filter = shift;
    my $freqinf = shift;
    my $freqsup = shift;
    my $contamination = shift;
    my $selected_attributes = @_; my @selected_attributes = @{$_[0]};
    my $outputFile = $temp_output_directory.'Analysed_platform1_pileup_1.txt';
    my $path_to_input = $input_directory.$inputFile;
    my $random = 10000;
    my (@cov, @highqualcov, @bqv_ratio, @mqv_ratio, @read_size);
    my ($med_cov, $med_highqualcov, $med_bqv_ratio, $med_mqv_ratio, $med_read_size);

    open (PU_FILE_INPUT, "<$path_to_input") or die "Cannot open file $path_to_input: $!";
    my $size=-s $path_to_input;
    while ($random--) {
      seek(PU_FILE_INPUT,int(rand($size)),0);
      my $l=readline(PU_FILE_INPUT);
      redo unless defined ($l = readline(PU_FILE_INPUT));

	my @l = split(/\t/, $l);
	    $l[4] =~ s/\^.//g;
	    $l[4] =~ tr/\!//d;
	    $l[4] =~ tr/\$//d;
	    $l[4] =~ tr/*/./;
	    $l[4] =~ tr/,/./;
	    next if $l[4] =~ /\+/;
	    next if $l[4] =~ /\-/;
	    $l[4] = uc $l[4];
	    
	    my @var; 	    
	    $var[0] = $l[4] =~ tr/A//;
	    $var[1] = $l[4] =~ tr/T//;
	    $var[2] = $l[4] =~ tr/G//;
	    $var[3] = $l[4] =~ tr/C//;
	    my $ref = $l[4] =~ tr/\.//;
	    
	    my @var_ord = sort { $a <=> $b } @var;
	    my $max = (sort @var_ord)[-1];
	    my $second = (sort @var_ord)[-2];
	    next if $max == $second;
	    push (@cov, $l[3]);
	    
	    my @N = ("A","T","G","C");
	    for (my $i=0;$i<4;$i++){
		if($max == $var[$i] && $var[$i]/$l[3] >= $freqinf && $var[$i]/$l[3] <= $freqsup){
		
			    #variables declaration
			    my $offset = 0;
			    my $refhighquality = 0;
			    my $varhighquality = 0;
			    my $highqualcov;
			    my $ref_mean_mqv_quality;
			    my $var_mean_mqv_quality;
			    my $bqv_ratio;
			    my $mqv_ratio;
			    my $ref_mean_bqv_quality; 
			    my $var_mean_bqv_quality;
			    my @readquality =();
			    my @basequality =();
			    my @refreadquality_val =();  
			    my @refbasequality_val =();
			    my @varreadquality_val =();  
			    my @varbasequality_val =();
			    			    
			    #Quality filter
			    my $refpos = index($l[4], ".", $offset);
			    my $varpos = index($l[4], $N[$i], $offset);
			    @basequality = split(//,$l[5]);
			    @basequality = map(ord $_, @basequality);
			    @readquality = split(//,$l[6]);
			    @readquality = map(ord $_, @readquality);
			    @read_size = split(/,/,$l[7]);
			 
			    #Ref eval
			    while ($refpos != -1) {
				    #High qual ref base count 
				    push (@refbasequality_val, $basequality[$refpos]);
				    #High qual ref read count
				    push (@refreadquality_val, $readquality[$refpos]);
				    if($basequality[$refpos]>=$bqv && $readquality[$refpos]>=$mqv){
					    $refhighquality ++;
				    }
				    $offset = $refpos + 1;
				    $refpos = index($l[4], ".", $offset);
			    }

			    #Var eval
			    while ($varpos != -1) {
				    #High qual var base count
				    push (@varbasequality_val, $basequality[$varpos]);
				    #High qual var read count
				    push (@varreadquality_val, $readquality[$varpos]);
				    if($basequality[$varpos]>=$bqv && $readquality[$varpos]>=$mqv){
					    $varhighquality ++;
				    }
				    $offset = $varpos + 1;
				    $varpos = index($l[4], $N[$i], $offset);
			    }
			    
			    $highqualcov = $refhighquality + $varhighquality;
			    push (@highqualcov, $highqualcov);
			    
			    ##Qual bias eval
			    #Base quality evaluation
			    $var_mean_bqv_quality = (sum(@varbasequality_val)/@varbasequality_val);
			    if(@refbasequality_val != 0){
				    $ref_mean_bqv_quality = (sum(@refbasequality_val)/@refbasequality_val);
				    $bqv_ratio = ($var_mean_bqv_quality/$ref_mean_bqv_quality);
				    push (@bqv_ratio, $bqv_ratio);
			    }
			    #Read quality evaluation
			    $var_mean_mqv_quality = (sum(@varreadquality_val)/@varreadquality_val);
			    if(@refreadquality_val != 0){
				    $ref_mean_mqv_quality = (sum(@refreadquality_val)/@refreadquality_val);
				    $mqv_ratio = ($var_mean_mqv_quality/$ref_mean_mqv_quality);
				    push (@mqv_ratio, $mqv_ratio);
			    }
		}
	     }
    }
    close PU_FILE_INPUT;
        
    ##############
    #coverage info
    ##############
    
    #calculation of the median
    @cov = sort { $a <=> $b } @cov;
    if( @cov % 2 == 0){
      my $sum = $cov[(@cov/2)-1] + $cov[(@cov/2)];
      $med_cov = $sum/2;
    }else{
      $med_cov = $cov[@cov/2];
    }
    
    #calculation of the median
    @highqualcov = sort { $a <=> $b } @highqualcov;
    if( @highqualcov % 2 == 0){
      my $sum = $highqualcov[(@highqualcov/2)-1] + $highqualcov[(@highqualcov/2)];
      $med_highqualcov = $sum/2;
    }else{
      $med_highqualcov = $highqualcov[@highqualcov/2];
    }
    
    ###############
    #bqv_ratio info
    ###############
    
    #calculation of the median
    @bqv_ratio = sort { $a <=> $b } @bqv_ratio;
    if( @bqv_ratio % 2 == 0){
      my $sum = $bqv_ratio[(@bqv_ratio/2)-1] + $bqv_ratio[(@bqv_ratio/2)];
      $med_bqv_ratio = $sum/2;
    }else{
      $med_bqv_ratio = $bqv_ratio[@bqv_ratio/2];
    }

    ###############
    #mqv_ratio info
    ###############
    
    #calculation of the median
    @mqv_ratio = sort { $a <=> $b } @mqv_ratio;
    if( @mqv_ratio % 2 == 0){
      my $sum = $mqv_ratio[(@mqv_ratio/2)-1] + $mqv_ratio[(@mqv_ratio/2)];
      $med_mqv_ratio = $sum/2;
    }else{
      $med_mqv_ratio = $mqv_ratio[@mqv_ratio/2];
    }
    
    ###############
    #read_size info
    ###############
    
    @read_size = sort { $a <=> $b } @read_size;
    if( @read_size % 2 == 0){
      my $sum = $read_size[(@read_size/2)-1] + $read_size[(@read_size/2)];
      $med_read_size = $sum/2;
    }else{
      $med_read_size = $read_size[@read_size/2];
    }
    
        
    #####################
    #Features acquisition
    #####################
    
    open (OUTPUTFILE, ">>$outputFile") or die "Cannot open file $outputFile: $!";
    open (PU_FILE_INPUT, "<$path_to_input") or die "Cannot open file $path_to_input: $!";    
    while (<PU_FILE_INPUT>){
	    chomp;
	    my @l = split(/\t/);
	    
	    #filter of special pileup character
	    $l[4] =~ s/\^.//g;
	    $l[4] =~ tr/\!//d;
	    $l[4] =~ tr/\$//d;
	    $l[4] =~ tr/*/./;
	    $l[2] = uc($l[2]);
	    
	    #filter of indels
	    #variables def     
	    my @motif_untrimmed;
	    my @motif;
	    my @motif_size;
	    my $indel_flag = 0;
	    #filter on
	    if($indel_filter eq "on"){
	      next if $l[4] =~ /\+/;
	      next if $l[4] =~ /\-/;
	    }else{
	     #get motif
	      @motif_untrimmed = ($l[4] =~ /[+|-]\d{1,2}[A|T|G|C|N]+/gi);
	      if(@motif_untrimmed != 0){
		$indel_flag = 1;
		foreach my $m(@motif_untrimmed){
		  if($m =~ /(\d{1,2})([A|T|G|C|N]+)/i){
		    my $motif_size = $1;
		    my $motif_untrimmed = $2;
		    my $motif = $motif_size.substr($motif_untrimmed,0,$motif_size);
		    #undefined suppression
		    $l[4] =~ s/[+|-]$motif//;
		  }
		}
	      }
	    }
	    #variant and ref counts
	    my @varR; my @varF; my @vartot;
		    
	    $varR[0] = $l[4] =~ tr/a//;
	    $varF[0] = $l[4] =~ tr/A//;
	    $vartot[0] = $varR[0]+$varF[0];
		    
	    $varR[1] = $l[4] =~ tr/t//;
	    $varF[1] = $l[4] =~ tr/T//;
	    $vartot[1] = $varR[1]+$varF[1];
		    
	    $varR[2] = $l[4] =~ tr/g//;
	    $varF[2] = $l[4] =~ tr/G//;
	    $vartot[2] = $varR[2]+$varF[2];
		    
	    $varR[3] = $l[4] =~ tr/c//;
	    $varF[3] = $l[4] =~ tr/C//;
	    $vartot[3] = $varR[3]+$varF[3];
	    
	    my @vartot_ord = sort { $a <=> $b } @vartot;
	    my $max = (sort @vartot_ord)[-1];
	    my $second = (sort @vartot_ord)[-2];
	    next if $max == $second;
	    
	    my $refR = $l[4] =~ tr/\,//;
	    my $refF = $l[4] =~ tr/\.//;
	    my $reftot = $refF + $refR;
	    
	#FILTER 1
	    my @N = ("A","T","G","C");
	    for (my $i=0;$i<4;$i++){
		    if($max == $vartot[$i] && $vartot[$i]/$l[3] >= $freqinf && $vartot[$i]/$l[3] <= $freqsup){
			    #variables declaration
			    my $offset = 0;
			    my $refRhighquality = 0;
			    my $refFhighquality = 0;
			    my $varRhighquality = 0;
			    my $varFhighquality = 0;
			    my $varhighquality = 0;
			    my $nb_of_considered_var;
			    my $coverage = $l[3];
			    my $highqualcov;
			    my $highqualcov_vs_med;
			    my $ref_mean_mqv_quality;
			    my $var_mean_mqv_quality;
			    my $ref_mean_bqv_quality; 
			    my $var_mean_bqv_quality;
			    my $allelic_freq_raw;
			    my $allelic_freq_highqual;
			    my $pbinom_sb;
			    my $pbinom_sb_qual;
			    my $SBflag2_qual;
			    my $FRratio;
			    my $FRratio_highqual;
			    my $right_value_sb;
			    my $right_value_sb_qual;
			    my $bqv_ratio_vs_med;
			    my $mqv_ratio_vs_med;
			    my $right_value_qual;
			    my $wilcox_gqual;
			    my $wilcox_rqual;
			    my $varbase_meanloc_vs_med;
			    my $varbase_meanloc_vs_ref;
			    my @refreadquality_val =(); 
			    my @refbasequality_val =();
			    my @refbase_loc =();
			    my @varreadquality_val =(); 
			    my @varbasequality_val =();
			    my @varbase_loc =();
			    
			    #Coverage evaluation
			    my $coverage_vs_med = sprintf '%4.4f', $coverage/$med_cov;

			    #Quality filter
			    my $refposR = index($l[4], ",", $offset);
			    my $refposF = index($l[4], ".", $offset);
			    my $varR = lc $N[$i];
			    my $varF = $N[$i];
			    my $varposR = index($l[4], "$varR", $offset);
			    my $varposF = index($l[4], "$varF", $offset);
			    my @basequality = split(//,$l[5]);
			    @basequality = map(ord $_, @basequality);
			    my @readquality = split(//,$l[6]);
			    @readquality = map(ord $_, @readquality);
			    my @baseloc = split(/,/,$l[7]);
			 
			    #Ref Reverse eval
			    while ($refposR != -1) {
				    #High qual ref base count 
				    push (@refbasequality_val, $basequality[$refposR]);
				    #High qual ref read count
				    push (@refreadquality_val, $readquality[$refposR]);
				    #High quality ref count(high qual ref read + high qual base ref) 
				    if($basequality[$refposR]>=$bqv && $readquality[$refposR]>=$mqv){
					    $refRhighquality ++;
				    }
				    #Location of the base in the read
				    push (@refbase_loc, sprintf '%1.4f', $baseloc[$refposR]/$med_read_size);
				    $offset = $refposR + 1;
				    $refposR = index($l[4], ",", $offset);
			    }
			    
			    #Ref Forward eval
			    while ($refposF != -1) {
				    #High qual ref base count 
				    push (@refbasequality_val, $basequality[$refposF]);
				    #High qual ref read count
				    push (@refreadquality_val, $readquality[$refposF]);
				    #High quality ref count(high qual ref read + high qual base ref) 
				    if($basequality[$refposF]>=$bqv && $readquality[$refposF]>=$mqv){
					    $refFhighquality ++;
				    }
				    #Location of the base in the read
				    push (@refbase_loc, sprintf '%1.4f', $baseloc[$refposF]/$med_read_size);
				    $offset = $refposF + 1;
				    $refposF = index($l[4], ".", $offset);
			    }
			    
			    #Var Reverse eval
			    while ($varposR != -1) {
				    #High qual var base count
				    push (@varbasequality_val, $basequality[$varposR]);
				    #High qual var read count
				    push (@varreadquality_val, $readquality[$varposR]);
				    #High quality var count(high qual var read + high qual base var) 
				    if($basequality[$varposR]>=$bqv && $readquality[$varposR]>=$mqv){
					    $varRhighquality ++;
				    }
				    #Location of the base in the read
				    push (@varbase_loc, sprintf '%1.4f', $baseloc[$varposR]/$med_read_size);
				    $offset = $varposR + 1;
				    $varposR = index($l[4], "$varR", $offset);
			    }
			    
			    #Var Forward eval
			    while ($varposF != -1) {
				    #High qual var base count
				    push (@varbasequality_val, $basequality[$varposF]);
				    #High qual var read count
				    push (@varreadquality_val, $readquality[$varposF]);
				    # High quality var count(high qual var read + high qual base var) 
				    if($basequality[$varposF]>=$bqv && $readquality[$varposF]>=$mqv){
					    $varFhighquality ++;
				    }
				    #Location of the base in the read
				    push (@varbase_loc, sprintf '%1.4f', $baseloc[$varposF]/$med_read_size);
				    $offset = $varposF + 1;
				    $varposF = index($l[4], "$varF", $offset);
			    }
			    $varhighquality = $varRhighquality + $varFhighquality;
	#FILTER 2		
			    if($qual_filter =~ /on/){
			      $nb_of_considered_var = $varhighquality;
			    }else{
			      $nb_of_considered_var = $vartot[$i];
			    }
			    
			    if(($nb_of_considered_var >= $nbvar_filter) && ($coverage >= $coverage_filter)){
				    my $start = $l[1]-1; my $end = $l[1];
				    $l[2] = uc($l[2]);
				    
				    #High qual coverage evaluation 
				    $highqualcov = $refRhighquality + $refFhighquality + $varhighquality;
				    #High qual coverage evaluation 
				    $highqualcov_vs_med = sprintf '%4.4f', $highqualcov/$med_highqualcov;
				    
				    ##Strand bias eval (no qual filter)  
				    if($varR[$i] != 0 && $varF[$i] != 0){
				      if($varR[$i] >= $varF[$i]){
					$FRratio = sprintf '%4.4f', $varR[$i]/$varF[$i];
				      }else{
					$FRratio = sprintf '%4.4f', $varF[$i]/$varR[$i];
				      }
				    }else{
				      $FRratio = '?';
				    }
				    
				    #Fisher test
				    #n1p = total of reads F+R for var
				    #np1 = total of reads F for ref and var
				    #n11 = total of reads F for var
				    #npp = coverage
				    my $errorCode_sb;
				    my $n1p_sb = $varR[$i] + $varF[$i];
				    my $np1_sb = $refF + $varF[$i];
				    my $np2_sb = $refR + $varR[$i];
				    my $n11_sb = $varF[$i];
				    my $npp_sb = $coverage;
				    
				    if(($refR + $refF) != 0){
				      $right_value_sb = sprintf '%1.4f', calculateStatistic(  n11=>$n11_sb,
											      n1p=>$n1p_sb,
											      np1=>$np1_sb,
											      npp=>$npp_sb );
				      if( ($errorCode_sb = getErrorCode())){
					  print STDERR $errorCode_sb." - ".getErrorMessage();
				      }
				    }else{
				      $right_value_sb = '?';
				    }
				     
				    #Cumulative probabilities from the Binomial distribution. 
				    #Probability of having n reads F or R for a coverage of N reads when each read has a probability of 0.5 to be F or R.
				    if($np2_sb < $np1_sb){
				      $pbinom_sb = sprintf '%1.4f', pbinom( $np2_sb, $coverage, 0.5 );
				    }else{
				      $pbinom_sb = sprintf '%1.4f', pbinom( $np1_sb, $coverage, 0.5 );
				    }

				    ##Strand bias eval (qual filter)  
				      if($varRhighquality != 0 && $varFhighquality != 0){
					if($varRhighquality >= $varFhighquality){
					  $FRratio_highqual = sprintf '%4.4f', $varRhighquality/$varFhighquality;
					}else{
					  $FRratio_highqual = sprintf '%4.4f', $varFhighquality/$varRhighquality;
					}
				      }else{
					$FRratio_highqual = '?';
				      }
				      
				      #Fisher test
				      my $errorCode_sb_qual;
				      my $n1p_sb_qual = $varhighquality;
				      my $np1_sb_qual = $refFhighquality + $varFhighquality;
				      my $np2_sb_qual = $refRhighquality + $varRhighquality;
				      my $n11_sb_qual = $varFhighquality;
				      my $npp_sb_qual = $highqualcov;
				      
				      if(($refRhighquality + $refFhighquality) != 0){
					$right_value_sb_qual = sprintf '%1.4f', calculateStatistic( n11=>$n11_sb_qual,
												    n1p=>$n1p_sb_qual,
												    np1=>$np1_sb_qual,
												    npp=>$npp_sb_qual );
					if( ($errorCode_sb_qual = getErrorCode())){
					  print STDERR $errorCode_sb_qual." - ".getErrorMessage();
					}
				      }else{
					$right_value_sb_qual = '?';
				      }
				     
				      #Binomial distribution
				      if($np2_sb_qual < $np1_sb_qual){
					$pbinom_sb_qual = sprintf '%1.4f', pbinom( $np2_sb_qual, $highqualcov, 0.5 );
				      }else{
					$pbinom_sb_qual = sprintf '%1.4f', pbinom( $np1_sb_qual, $highqualcov, 0.5 );
				      }
				    
				    ##Qual bias eval
				      #Base quality evaluation
				      $var_mean_bqv_quality = sprintf '%4.4f', (sum(@varbasequality_val)/@varbasequality_val);
				      if(@refbasequality_val != 0){
					$ref_mean_bqv_quality = sprintf '%4.4f', (sum(@refbasequality_val)/@refbasequality_val);
					$bqv_ratio_vs_med = sprintf '%4.4f', ($var_mean_bqv_quality/$ref_mean_bqv_quality)/$med_bqv_ratio;
				      }else{
					$bqv_ratio_vs_med = '?';
				      }
				      
				      #Read quality evaluation
				      $var_mean_mqv_quality = sprintf '%4.4f', (sum(@varreadquality_val)/@varreadquality_val);
				      if(@refreadquality_val != 0){
					$ref_mean_mqv_quality = sprintf '%4.4f', (sum(@refreadquality_val)/@refreadquality_val);
					$mqv_ratio_vs_med = sprintf '%4.4f', ($var_mean_mqv_quality/$ref_mean_mqv_quality)/$med_mqv_ratio;
				      }else{
					$mqv_ratio_vs_med = '?';
				      }
				      
				      #Fisher test
				      #n1p = total of ref passing or not the qual filter
				      #np1 = total of bases passing the qual filter for ref and var
				      #n11 = total of ref passing the qual filter
				      #npp = coverage
				      my $errorCode_qual;
				      my $n1p_qual = $reftot;
				      my $np1_qual = $refRhighquality + $refFhighquality + $varhighquality;
				      my $n11_qual = $refRhighquality + $refFhighquality;
				      my $npp_qual = $coverage;
				      
				      if(($refRhighquality + $refFhighquality) != 0){
					$right_value_qual = sprintf '%1.4f', calculateStatistic( n11=>$n11_qual,
												 n1p=>$n1p_qual,
												 np1=>$np1_qual,
												 npp=>$npp_qual );
					if( ($errorCode_qual = getErrorCode())){
					    print STDERR $errorCode_qual." - ".getErrorMessage();
					}
				      }else{
					$right_value_qual = '?';
				      }
				      
				      #Wilcoxon rank test
				      my $wilcox_test = Statistics::Test::WilcoxonRankSum->new();
				      
				      #Base qual bias
				      if(@refbasequality_val != 0){
					$wilcox_test->load_data(\@refbasequality_val, \@varbasequality_val);
					$wilcox_gqual = sprintf '%1.4f', $wilcox_test->probability();
				      }else{
					$wilcox_gqual = '?';
				      }
				      
				      #Read qual bias
				      if(@refreadquality_val != 0){
					$wilcox_test->load_data(\@refreadquality_val, \@varreadquality_val);
					$wilcox_rqual = sprintf '%1.4f', $wilcox_test->probability();
				      }else{
					$wilcox_rqual = '?';
				      }
				      
				    ##Relative location in the read bias
				      $varbase_meanloc_vs_med = sprintf '%1.4f', (sum(@varbase_loc)/@varbase_loc);
				      if(@refreadquality_val != 0){
					$varbase_meanloc_vs_ref = sprintf '%1.4f', $varbase_meanloc_vs_med - (sum(@refbase_loc)/@refbase_loc);
				      }else{
					$varbase_meanloc_vs_ref = '?';
				      }
				      
				    #Raw allelic frequency evaluation
				    $allelic_freq_raw = sprintf '%1.4f', $vartot[$i]/($vartot[$i]+$reftot);
				    if($contamination != 0){
				      $allelic_freq_raw = sprintf '%1.4f', $allelic_freq_raw/$contamination;
				      if($allelic_freq_raw > 1){
					$allelic_freq_raw = 1;
				      }
				    }

				    #High qual allelic frequency
				    if($highqualcov != 0){
				      $allelic_freq_highqual = sprintf '%1.4f', ($varRhighquality + $varFhighquality)/$highqualcov;
				      if($contamination != 0){
					$allelic_freq_highqual = sprintf '%1.4f', $allelic_freq_highqual/$contamination;
					if($allelic_freq_highqual > 1){
					  $allelic_freq_highqual = 1;
					}
				      }
				    }
				   
				    
				    print (OUTPUTFILE "$l[0]\t$start\t$end\t$sample_id,$l[2],$N[$i],$coverage,$vartot[$i],$highqualcov,$varhighquality");
				    
				    my %attributes_hash = (
				      coverage_vs_med => $coverage_vs_med,
				      highqualcov_vs_med => $highqualcov_vs_med,
				      LocMean_vs_med => $varbase_meanloc_vs_med,
				      LocMean_vs_ref => $varbase_meanloc_vs_ref,
				      indelflag => $indel_flag,
				      FRratio => $FRratio,
				      SBpval => $right_value_sb,
				      SBpbinom => $pbinom_sb,
				      FRratioqual => $FRratio_highqual,
				      SBpvalqual => $right_value_sb_qual,
				      SBpbinomqual => $pbinom_sb_qual,
				      var_mean_bqv_quality => $var_mean_bqv_quality,
				      bqv_ratio_vs_med => $bqv_ratio_vs_med,
				      var_mean_mqv_quality => $var_mean_mqv_quality,
				      mqv_ratio_vs_med => $mqv_ratio_vs_med,
				      QBpval => $right_value_qual,
				      GQBprob => $wilcox_gqual,
				      RQBprob => $wilcox_rqual,
				      allelic_freq_raw => $allelic_freq_raw,
				      allelic_freq_highqual => $allelic_freq_highqual
				    );
				    
				    foreach my $attributes(@selected_attributes){
				      print (OUTPUTFILE ",".$attributes_hash{$attributes});
				    } 
				    print (OUTPUTFILE "\n");
			    }
		    }
	    }
    }
    close OUTPUTFILE; close PU_FILE_INPUT;

  return();
}


sub PileupIndelAnalysis{

    #variables declaration
    my $input_directory = shift;
    my $inputFile = shift;
    my $temp_output_directory = shift;
    my $sample_id = shift;
    my $bqv = shift;
    my $mqv = shift;
    my $nbvar_filter = shift;
    my $coverage_filter = shift;
    my $qual_filter = shift;
    my $contamination = shift;
    my $selected_attributes = @_; my @selected_attributes = @{$_[0]};
    my $outputFile = $temp_output_directory.'Analysed_platform1_pileup_1.txt';
    my $path_to_input = $input_directory.$inputFile;
    my $random = 100000;
    my (@cov, @highqualcov, @bqv_ratio, @mqv_ratio, @read_size);
    my ($med_cov, $med_highqualcov, $med_bqv_ratio, $med_mqv_ratio, $med_read_size);
    
    open (PU_FILE_INPUT, "<$path_to_input") or die "Cannot open file $path_to_input: $!";
    my $size=-s $path_to_input;
    while ($random--) {
      seek(PU_FILE_INPUT,int(rand($size)),0);
      my $l=readline(PU_FILE_INPUT);
      redo unless defined ($l = readline(PU_FILE_INPUT));
      my @l = split(/\t/, $l);
      
	     if($l[4] =~ /[+|-]\d{1,2}[A|T|G|C]+/){   

	      #filter of special pileup characters
	      $l[4] =~ s/\^.//g;
	      $l[4] =~ tr/\!//d;
	      $l[4] =~ tr/\$//d;
	      $l[4] =~ tr/*/./;
	      $l[4] =~ tr/,/./;
	      $l[4] = uc $l[4];
	      
	      #filter of positions with insertion and deletion or undefined
	      next if($l[4] =~ /\+/ && $l[4] =~ /\-/);
	      next if $l[4] =~ /N/;
	      $l[4] =~ tr/\-/\+/;

	      # INDELs analysis
			#variables def     
			my @indpos;
			my @motif_untrimmed;
			my @motif;
			my @motif_size;
			my $motif;
			my $offset = 0;
			push (@cov, $l[3]);
			
			@motif_untrimmed = ($l[4] =~ /[+]\d{1,2}[A|T|G|C]+/g);
			foreach my $m(@motif_untrimmed){
			  if($m =~ /(\d{1,2})([A|T|G|C]+)/){
			    my $motif_size = $1;
			    push(@motif_size, $motif_size);
			    my $motif_untrimmed = $2;
			    push(@motif_untrimmed, $motif_untrimmed);
			    if(substr($motif_untrimmed,0,$motif_size) =~ /[A|T|G|C]/){
			      push(@motif, substr($motif_untrimmed,0,$motif_size));
			    }
			  }
			}
			#get motif
			if(scalar(@motif) != 0){
			  $motif = $motif[0];
			  next if(keys %{{ map {$_, 1} @motif }} != 1);
			}
			#motif size
			my $motif_size = length($motif);
			 
			#replacement of the character before the motif to track it
			my $indpos = index($l[4],"+".$motif_size.$motif, $offset);
			while ($indpos != -1) {
			  push (@indpos, $indpos);
			  $offset = $indpos + 1;
			  $indpos = index($l[4],"+".$motif_size.$motif, $offset);
			}
			foreach $indpos(@indpos){
			  my $fragment = substr($l[4],($indpos-1),1,'X');
			}
			  
			#suppression of the motif
			foreach my $m(@motif){
			  $l[4] =~ s/[+]\d{1,2}$m//;
			}
			
			#variables declaration
			    $offset = 0;
			    my $refhighquality = 0;
			    my $varhighquality = 0;
			    my $highqualcov;
			    my $ref_mean_mqv_quality;
			    my $var_mean_mqv_quality;
			    my $bqv_ratio;
			    my $mqv_ratio;
			    my $ref_mean_bqv_quality; 
			    my $var_mean_bqv_quality;
			    my @readquality =();
			    my @basequality =();
			    my @refreadquality_val =(); 
			    my @refbasequality_val =();
			    my @varreadquality_val =(); 
			    my @varbasequality_val =();
			    			    
			    #Quality filter
			    my $refpos = index($l[4], ".", $offset);
			    my $varpos = index($l[4], "X", $offset);
			    @basequality = split(//,$l[5]);
			    @basequality = map(ord $_, @basequality);
			    @readquality = split(//,$l[6]);
			    @readquality = map(ord $_, @readquality);
			    @read_size = split(/,/,$l[7]);
			 
			    #Ref eval
			    while ($refpos != -1) {
				    #High qual ref base count 
				    push (@refbasequality_val, $basequality[$refpos]);
				    #High qual ref read count
				    push (@refreadquality_val, $readquality[$refpos]);
				    if($basequality[$refpos]>=$bqv && $readquality[$refpos]>=$mqv){
					    $refhighquality ++;
				    }
				    $offset = $refpos + 1;
				    $refpos = index($l[4], ".", $offset);
			    }

			    #Var eval
			    while ($varpos != -1) {
				    #High qual var base count
				    push (@varbasequality_val, $basequality[$varpos]);
				    #High qual var read count
				    push (@varreadquality_val, $readquality[$varpos]);
				    if($basequality[$varpos]>=$bqv && $readquality[$varpos]>=$mqv){
					    $varhighquality ++;
				    }
				    $offset = $varpos + 1;
				    $varpos = index($l[4], "X", $offset);
			    }
			    
			    $highqualcov = $refhighquality + $varhighquality;
			    push (@highqualcov, $highqualcov);
			    
			    ##Qual bias eval
			    #Base quality evaluation
			    $var_mean_bqv_quality = (sum(@varbasequality_val)/@varbasequality_val);
			    if(@refbasequality_val != 0){
				    $ref_mean_bqv_quality = (sum(@refbasequality_val)/@refbasequality_val);
				    $bqv_ratio = ($var_mean_bqv_quality/$ref_mean_bqv_quality);
				    push (@bqv_ratio, $bqv_ratio);
			    }
			    #Read quality evaluation
			    $var_mean_mqv_quality = (sum(@varreadquality_val)/@varreadquality_val);
			    if(@refreadquality_val != 0){
				    $ref_mean_mqv_quality = (sum(@refreadquality_val)/@refreadquality_val);
				    $mqv_ratio = ($var_mean_mqv_quality/$ref_mean_mqv_quality);
				    push (@mqv_ratio, $mqv_ratio);
			    }
		}
    }
    close PU_FILE_INPUT;
    
    ##############
    #coverage info
    ##############
    
    #calculation of the median
    @cov = sort { $a <=> $b } @cov;
    if( @cov % 2 == 0){
      my $sum = $cov[(@cov/2)-1] + $cov[(@cov/2)];
      $med_cov = $sum/2;
    }else{
      $med_cov = $cov[@cov/2];
    }
    
    #calculation of the median
    @highqualcov = sort { $a <=> $b } @highqualcov;
    if( @highqualcov % 2 == 0){
      my $sum = $highqualcov[(@highqualcov/2)-1] + $highqualcov[(@highqualcov/2)];
      $med_highqualcov = $sum/2;
    }else{
      $med_highqualcov = $highqualcov[@highqualcov/2];
    }
    
    #############
    #bqv_ratio info
    #############
    
    #calculation of the median
    @bqv_ratio = sort { $a <=> $b } @bqv_ratio;
    if( @bqv_ratio % 2 == 0){
      my $sum = $bqv_ratio[(@bqv_ratio/2)-1] + $bqv_ratio[(@bqv_ratio/2)];
      $med_bqv_ratio = $sum/2;
    }else{
      $med_bqv_ratio = $bqv_ratio[@bqv_ratio/2];
    }

    #############
    #mqv_ratio info
    #############
    
    #calculation of the median
    @mqv_ratio = sort { $a <=> $b } @mqv_ratio;
    if( @mqv_ratio % 2 == 0){
      my $sum = $mqv_ratio[(@mqv_ratio/2)-1] + $mqv_ratio[(@mqv_ratio/2)];
      $med_mqv_ratio = $sum/2;
    }else{
      $med_mqv_ratio = $mqv_ratio[@mqv_ratio/2];
    }
    
    ###############
    #read_size info
    ###############
    
    @read_size = sort { $a <=> $b } @read_size;
    if( @read_size % 2 == 0){
      my $sum = $read_size[(@read_size/2)-1] + $read_size[(@read_size/2)];
      $med_read_size = $sum/2;
    }else{
      $med_read_size = $read_size[@read_size/2];
    }
    
        
    #####################
    #Features acquisition
    #####################
    
    open (OUTPUTFILE, ">>$outputFile") or die "Cannot open file $outputFile: $!";
    open (PU_FILE_INPUT, "<$path_to_input") or die "Cannot open file $path_to_input: $!";    
    while (<PU_FILE_INPUT>){
	    chomp;
	    my @l = split(/\t/);
	    #filter of special pileup characters
	      $l[4] =~ s/\^.//g;
	      $l[4] =~ tr/\!//d;
	      $l[4] =~ tr/\$//d;
	      $l[4] =~ tr/*/./;
	      $l[2] = uc($l[2]);

		# INDELs analysis
		if($l[4] =~ /\+/ || $l[4] =~ /\-/){
			#variables def     
			my @insposF;
			my @delposF;
			my @insposR;
			my @delposR;
			my @motif_untrimmed;
			my @motif;
			my @motif2;
			my @motif_size;
			my $motif;
			
			#filter of positions with insertion and deletion
			my $countins= $l[4] =~ tr/\+//;
			my $countdel= $l[4] =~ tr/\-//;
			next if($countins != 0 && $countdel != 0);
			
			#filter of indels containing undifined bases (N) + forward and reverse info
			@motif_untrimmed = ($l[4] =~ /[+|-]\d{1,2}[A|T|G|C|N]+/gi);
			foreach my $m(@motif_untrimmed){
			  if($m =~ /(\d{1,2})([A|T|G|C|N]+)/i){
			    my $motif_size = $1;
			    push(@motif_size, $motif_size);
			    my $motif_untrimmed = $2;
			    push(@motif_untrimmed, $motif_untrimmed);
			    if(substr($motif_untrimmed,0,$motif_size) =~ /N/i){
			      my $motif = $motif_size.substr($motif_untrimmed,0,$motif_size);
			      #undefined suppression
			      $l[4] =~ s/[+|-]$motif//;
			    }elsif(substr($motif_untrimmed,0,$motif_size) =~ /[A|T|G|C]/){
			      push(@motif, substr($motif_untrimmed,0,$motif_size));
			    }elsif(substr($motif_untrimmed,0,$motif_size) =~ /[a|t|g|c]/){
			      push(@motif2, substr($motif_untrimmed,0,$motif_size));
			    }
			  }
			}
			
			#get motif
			next if(scalar(@motif) == 0 && scalar(@motif2) == 0);
			if(scalar(@motif) != 0 && scalar(@motif2) != 0){
			  @motif = (@motif, map { uc($_) } @motif2);
			  my %count;
			  ++$count{$_} for @motif;
			  $motif = (reverse sort { $count{$a} <=> $count{$b} } keys %count)[0]; 
			  if(keys %{{ map {$_, 1} @motif }} != 1){
			    my $second = (reverse sort { $count{$a} <=> $count{$b} } keys %count)[1];
			    next if $count{$motif} == $count{$second};
			  }
			}elsif(scalar(@motif) != 0){
			  my %count;
			  ++$count{$_} for @motif;
			  $motif = (reverse sort { $count{$a} <=> $count{$b} } keys %count)[0]; 
			  if(keys %{{ map {$_, 1} @motif }} != 1){
			    my $second = (reverse sort { $count{$a} <=> $count{$b} } keys %count)[1];
			    next if $count{$motif} == $count{$second};
			  }
			}elsif(scalar(@motif2) != 0){
			  my %count;
			  ++$count{$_} for @motif2;
			  $motif = (reverse sort { $count{$a} <=> $count{$b} } keys %count)[0]; 
			  if(keys %{{ map {$_, 1} @motif2 }} != 1){
			    my $second = (reverse sort { $count{$a} <=> $count{$b} } keys %count)[1];
			    next if $count{$motif} == $count{$second};
			  }
			}
			
			#motif size
			my $motif_size = length($motif);
			my $start = $l[1]-1;
			my $end = $start + $motif_size;
			 
			#replacement of the character before the motif to track it
			my $offset1 = 0; my $offset2 = 0;
			if($countins != 0){
			  my $insposR = index($l[4],"+".$motif_size.lc $motif, $offset1);
			  while ($insposR != -1) {
			    push (@insposR, $insposR);
			    $offset1 = $insposR + 1;
			    $insposR = index($l[4],"+".$motif_size.lc $motif, $offset1);
			  }
			  foreach $insposR(@insposR){
			    my $fragment = substr($l[4],($insposR-1),1,'x');
			  }  
			  my $insposF = index($l[4],"+".$motif_size.$motif, $offset2);
			  while ($insposF != -1) {
			    push (@insposF, $insposF);
			    $offset2 = $insposF + 1;
			    $insposF = index($l[4],"+".$motif_size.$motif, $offset2);
			  }
			  foreach $insposF(@insposF){
			    my $fragment = substr($l[4],($insposF-1),1,'X');
			  }

			}elsif($countdel != 0){
			  my $delposR = index($l[4],"-".$motif_size.lc $motif, $offset1);
			  while ($delposR != -1) {
			    push (@delposR, $delposR);
			    $offset1 = $delposR + 1;
			    $delposR = index($l[4],"-".$motif_size.lc $motif, $offset1);
			  }
			  foreach $delposR(@delposR){
			    my $fragment = substr($l[4],($delposR-1),1,'x');
			  }
			  my $delposF = index($l[4],"-".$motif_size.$motif, $offset2);
			  while ($delposF != -1) {
			    push (@delposF, $delposF);
			    $offset2 = $delposF + 1;
			    $delposF = index($l[4],"-".$motif_size.$motif, $offset2);
			  }
			  foreach $delposF(@delposF){
			    my $fragment = substr($l[4],($delposF-1),1,'X');
			  }
			  
			}

			#suppression of the motif
			foreach my $m(@motif){
			  $l[4] =~ s/[+|-]\d{1,2}$m//;
			}
			foreach my $m(@motif2){
			  $l[4] =~ s/[+|-]\d{1,2}$m//;
			}
			
			#ref count
			my $refR = $l[4] =~ tr/\,//;
			my $refF = $l[4] =~ tr/\.//;
			my $reftot = $refF + $refR;
			#var count
			my $varR = $l[4] =~ tr/x//;
			my $varF = $l[4] =~ tr/X//;
			my $vartot = $varR + $varF;

			#analysis of the base preceding the indel 
			my $offset = 0;
			my $refRhighquality = 0;
			my $refFhighquality = 0;
			my $varRhighquality = 0;
			my $varFhighquality = 0;
			my $varhighquality = 0;
			my $nb_of_considered_var;
			my $coverage = $l[3];
			my $highqualcov;
			my $highqualcov_vs_med;
			my $ref_mean_mqv_quality;
			my $var_mean_mqv_quality;
			my $ref_mean_bqv_quality; 
			my $var_mean_bqv_quality;
			my $allelic_freq_raw;
			my $allelic_freq_highqual;
			my $pbinom_sb;
			my $pbinom_sb_qual;
			my $FRratio;
			my $FRratio_highqual;
			my $right_value_sb;
			my $right_value_sb_qual;
			my $bqv_ratio_vs_med;
			my $mqv_ratio_vs_med;
			my $right_value_qual;
			my $wilcox_gqual;
			my $wilcox_rqual;
			my $varbase_meanloc_vs_med;
			my $varbase_meanloc_vs_ref;
			my @refreadquality_val =();  
			my @refbasequality_val =();
			my @refbase_loc =();
			my @varreadquality_val =();  
			my @varbasequality_val =();
			my @varbase_loc =();
			
			#Coverage evaluation
			my $coverage_vs_med = sprintf '%4.4f', $coverage/$med_cov;
			    
			#Var-Ref location
			my $refposR = index($l[4], ",", $offset);
			my $refposF = index($l[4], ".", $offset);
			my $varposR = index($l[4], "x", $offset);
			my $varposF = index($l[4], "X", $offset);
			my @basequality = split(//,$l[5]);
			@basequality = map(ord $_, @basequality);
			my @readquality = split(//,$l[6]);
			@readquality = map(ord $_, @readquality);
			my @baseloc = split(/,/,$l[7]);
			    
			#Ref Reverse eval
			while ($refposR != -1) {
				    #High qual ref base count 
				    push (@refbasequality_val, $basequality[$refposR]);
				    #High qual ref read count
				    push (@refreadquality_val, $readquality[$refposR]);
				    #High quality ref count(high qual ref read + high qual base ref) 	
				    if($basequality[$refposR]>=$bqv && $readquality[$refposR]>=$mqv){
					    $refRhighquality ++;
				    }
				    #Location of the base in the read
				    push (@refbase_loc, sprintf '%1.4f', $baseloc[$refposR]/$med_read_size);
				    $offset = $refposR + 1;
				    $refposR = index($l[4], ",", $offset);
			}
			
			#Ref Forward eval
			while ($refposF != -1) {
				    #High qual ref base count 
				    push (@refbasequality_val, $basequality[$refposF]);
				    #High qual ref read count
				    push (@refreadquality_val, $readquality[$refposF]);
				    #High quality ref count(high qual ref read + high qual base ref) 	
				    if($basequality[$refposF]>=$bqv && $readquality[$refposF]>=$mqv){
					    $refFhighquality ++;
				    }
				    #Location of the base in the read
				    push (@refbase_loc, sprintf '%1.4f', $baseloc[$refposF]/$med_read_size);
				    $offset = $refposF + 1;
				    $refposF = index($l[4], ".", $offset);
			}
			
			#Var Reverse eval
			while ($varposR != -1) {
				    #High qual var base count
				    push (@varbasequality_val, $basequality[$varposR]);
				    #High qual var read count
				    push (@varreadquality_val, $readquality[$varposR]);
				    # High quality var count(high qual var read + high qual base var) 	
				    if($basequality[$varposR]>=$bqv && $readquality[$varposR]>=$mqv){
					    $varRhighquality ++;
				    }
				    #Location of the base in the read
				    push (@varbase_loc, sprintf '%1.4f', $baseloc[$varposR]/$med_read_size);
				    $offset = $varposR + 1;
				    $varposR = index($l[4], "x", $offset);
			}
			    
			#Var Forward eval
			while ($varposF != -1) {
				    #High qual var base count    
				    push (@varbasequality_val, $basequality[$varposF]);
				    #High qual var read count
				    push (@varreadquality_val, $readquality[$varposF]);
				    # High quality var count(high qual var read + high qual base var) 	
				    if($basequality[$varposF]>=$bqv && $readquality[$varposF]>=$mqv){
					    $varFhighquality ++;
				    }
				    #Location of the base in the read
				    push (@varbase_loc, sprintf '%1.4f', $baseloc[$varposF]/$med_read_size);
				    $offset = $varposF + 1;
				    $varposF = index($l[4], "X", $offset);
			}
			$varhighquality = $varRhighquality + $varFhighquality;
			
			
			#FILTER 2		
			    if($qual_filter =~ /on/){
			      $nb_of_considered_var = $varhighquality;
			    }else{
			      $nb_of_considered_var = $vartot;
			    }
			    if(($nb_of_considered_var >= $nbvar_filter) && ($coverage >= $coverage_filter)){
				    
				    #High qual coverage evaluation 
				    $highqualcov = $refRhighquality + $refFhighquality + $varhighquality;
				    #High qual coverage evaluation 
				    $highqualcov_vs_med = sprintf '%4.4f', $highqualcov/$med_highqualcov;
				    
				    ##Strand bias eval (no qual filter)  
				    if($varR != 0 && $varF != 0){
				      if($varR >= $varF){
					$FRratio = sprintf '%4.4f', $varR/$varF;
				      }else{
					$FRratio = sprintf '%4.4f', $varF/$varR;
				      }
				    }else{
				      $FRratio = '?';
				    }
				    
				    #Fisher test
				    #n1p = total of reads F+R for var
				    #np1 = total of reads F for ref and var
				    #n11 = total of reads F for var
				    #npp = coverage
				    my $errorCode_sb;
				    my $n1p_sb = $varR + $varF;
				    my $np1_sb = $refF + $varF;
				    my $np2_sb = $refR + $varR;
				    my $n11_sb = $varF;
				    my $npp_sb = $coverage;
				    my $right_value_sb = sprintf '%1.4f', calculateStatistic( n11=>$n11_sb,
											      n1p=>$n1p_sb,
											      np1=>$np1_sb,
											      npp=>$npp_sb );
				    if( ($errorCode_sb = getErrorCode())){
					print STDERR $errorCode_sb." - ".getErrorMessage();
				    }
				    
				    #Cumulative probabilities from the Binomial distribution (coverage). 
				    #Probability of having n reads F or R for a coverage of N reads when each read has a probability of 0.5 to be F or R.
				    if($np2_sb < $np1_sb){
				      $pbinom_sb = sprintf '%1.4f', pbinom( $np2_sb, $coverage, 0.5 );
				    }else{
				      $pbinom_sb = sprintf '%1.4f', pbinom( $np1_sb, $coverage, 0.5 );
				    }
				    
				    ##Strand bias eval (qual filter)  
				      if($varRhighquality != 0 && $varFhighquality != 0){
					if($varRhighquality >= $varFhighquality){
					  $FRratio_highqual = sprintf '%4.4f', $varRhighquality/$varFhighquality;
					}else{
					  $FRratio_highqual = sprintf '%4.4f', $varFhighquality/$varRhighquality;
					}
				      }else{
					$FRratio_highqual = '?';
				      }
				      
				      #Fisher test
				      my $errorCode_sb_qual;
				      my $n1p_sb_qual = $varhighquality;
				      my $np1_sb_qual = $refFhighquality + $varFhighquality;
				      my $np2_sb_qual = $refRhighquality + $varRhighquality;
				      my $n11_sb_qual = $varFhighquality;
				      my $npp_sb_qual = $highqualcov;
				      
				      if(($refRhighquality + $refFhighquality) !=0){
					$right_value_sb_qual = sprintf '%1.4f', calculateStatistic( n11=>$n11_sb_qual,
												    n1p=>$n1p_sb_qual,
												    np1=>$np1_sb_qual,
												    npp=>$npp_sb_qual );
					if( ($errorCode_sb_qual = getErrorCode())){
					    print STDERR $errorCode_sb_qual." - ".getErrorMessage();
					}
				      }else{
					$right_value_sb_qual = '?';
				      }
				      
				      #Binomial distribution (coverage)
				      if($np2_sb_qual < $np1_sb_qual){
					$pbinom_sb_qual = sprintf '%1.4f', pbinom( $np2_sb_qual, $highqualcov, 0.5 );
				      }else{
					$pbinom_sb_qual = sprintf '%1.4f', pbinom( $np1_sb_qual, $highqualcov, 0.5 );
				      }
				    
				    ##Qual bias eval
				      #Base quality evaluation
				      $var_mean_bqv_quality = sprintf '%4.4f', (sum(@varbasequality_val)/@varbasequality_val);
				      if(@refbasequality_val != 0){
					$ref_mean_bqv_quality = sprintf '%4.4f', (sum(@refbasequality_val)/@refbasequality_val);
					$bqv_ratio_vs_med = sprintf '%4.4f', ($var_mean_bqv_quality/$ref_mean_bqv_quality)/$med_bqv_ratio;
				      }else{
					$bqv_ratio_vs_med = '?';
				      }
				      
				      #Read quality evaluation
				      $var_mean_mqv_quality = sprintf '%4.4f', (sum(@varreadquality_val)/@varreadquality_val);
				      if(@refreadquality_val != 0){
					$ref_mean_mqv_quality = sprintf '%4.4f', (sum(@refreadquality_val)/@refreadquality_val);
					$mqv_ratio_vs_med = sprintf '%4.4f', ($var_mean_mqv_quality/$ref_mean_mqv_quality)/$med_mqv_ratio;
				      }else{
					$mqv_ratio_vs_med = '?';
				      }
				      
				      #Fisher test
				      #n1p = total of ref passing or not the qual filter
				      #np1 = total of bases passing the qual filter for ref and var
				      #n11 = total of ref passing the qual filter
				      #npp = coverage
				      my $errorCode_qual;
				      my $n1p_qual = $reftot;
				      my $np1_qual = $refRhighquality + $refFhighquality + $varhighquality;
				      my $n11_qual = $refRhighquality + $refFhighquality;
				      my $npp_qual = $coverage;
				      
				      if(($refRhighquality + $refFhighquality) != 0){
					$right_value_qual = sprintf '%1.4f', calculateStatistic( n11=>$n11_qual,
												n1p=>$n1p_qual,
												np1=>$np1_qual,
												npp=>$npp_qual );
					if( ($errorCode_qual = getErrorCode())){
					    print STDERR $errorCode_qual." - ".getErrorMessage();
					}
				      }else{
					$right_value_qual = '?';
				      }
				      
				      #Wilcoxon rank test
				      my $wilcox_test = Statistics::Test::WilcoxonRankSum->new();
					
				      #Base qual bias
				      if(@refbasequality_val != 0){
					$wilcox_test->load_data(\@refbasequality_val, \@varbasequality_val);
					$wilcox_gqual = sprintf '%1.4f', $wilcox_test->probability();
				      }else{
					$wilcox_gqual = '?';
				      }
					
				      #Read qual bias
				      if(@refreadquality_val != 0){
					$wilcox_test->load_data(\@refreadquality_val, \@varreadquality_val);
					$wilcox_rqual = sprintf '%1.4f', $wilcox_test->probability();
				      }else{
					$wilcox_rqual = '?';
				      }
				    
				    ##Relative location in the read bias
				      $varbase_meanloc_vs_med = sprintf '%1.4f', (sum(@varbase_loc)/@varbase_loc);
				      if(@refreadquality_val != 0){
					$varbase_meanloc_vs_ref = sprintf '%1.4f', $varbase_meanloc_vs_med - (sum(@refbase_loc)/@refbase_loc);
				      }else{
					$varbase_meanloc_vs_ref = '?';
				      }

				    #Raw allelic frequency evaluation
				    $allelic_freq_raw = sprintf '%1.4f', $vartot/($vartot+$reftot);
				    if($contamination != 0){
				      $allelic_freq_raw = sprintf '%1.4f', $allelic_freq_raw/$contamination;
				      if($allelic_freq_raw > 1){
					$allelic_freq_raw = 1;
				      }
				    }

				    #High qual allelic frequency
				    if($highqualcov != 0){
				      $allelic_freq_highqual = sprintf '%1.4f', ($varRhighquality + $varFhighquality)/$highqualcov;
				      if($contamination != 0){
					$allelic_freq_highqual = sprintf '%1.4f', $allelic_freq_highqual/$contamination;
					if($allelic_freq_highqual > 1){
					  $allelic_freq_highqual = 1;
					}
				      }
				    }
				      
				    if($countins > 0){
				      print (OUTPUTFILE "$l[0]\t$start\t$l[1]\t$sample_id,-,$motif,$coverage,$vartot,$highqualcov,$varhighquality");
				    } elsif($countdel > 0){
				      print (OUTPUTFILE "$l[0]\t$start\t$end\t$sample_id,$motif,-,$coverage,$vartot,$highqualcov,$varhighquality");
				    }

				    my %attributes_hash = (
				      coverage_vs_med => $coverage_vs_med,
				      highqualcov_vs_med => $highqualcov_vs_med,
				      LocMean_vs_med => $varbase_meanloc_vs_med,
				      LocMean_vs_ref => $varbase_meanloc_vs_ref,
				      FRratio => $FRratio,
				      SBpval => $right_value_sb,
				      SBpbinom => $pbinom_sb,
				      FRratioqual => $FRratio_highqual,
				      SBpvalqual => $right_value_sb_qual,
				      SBpbinomqual => $pbinom_sb_qual,
				      var_mean_bqv_quality => $var_mean_bqv_quality,
				      bqv_ratio_vs_med => $bqv_ratio_vs_med,
				      var_mean_mqv_quality => $var_mean_mqv_quality,
				      mqv_ratio_vs_med => $mqv_ratio_vs_med,
				      QBpval => $right_value_qual,
				      GQBprob => $wilcox_gqual,
				      RQBprob => $wilcox_rqual,
				      allelic_freq_raw => $allelic_freq_raw,
				      allelic_freq_highqual => $allelic_freq_highqual
				    );
					  
				    foreach my $attributes(@selected_attributes){
				      print (OUTPUTFILE ",".$attributes_hash{$attributes});
				    } 
				    print (OUTPUTFILE "\n");
			    }
		    }
    }
    close OUTPUTFILE; close PU_FILE_INPUT;

  return();
}



#============================================
#              germDB filter
#============================================

sub FiltergermDB{  

   my $temp_output_directory = shift;
   my $path_to_bedtool = shift;
   my $path_to_germDB = shift; 
   my $inputFile = $temp_output_directory.'Analysed_platform1_pileup_1.txt';
   my $outputFile = $temp_output_directory.'Analysed_platform1_pileup_2.txt';
   
   # Bedtools function to remove the intersection between 2 bed files (germline variants database)
   `$path_to_bedtool intersect -a $inputFile -b $path_to_germDB -v > $outputFile`;
   `rm $inputFile`;
   
   return();
}



#============================================
#            BlackList filter
#============================================

sub BlackList{  
   
   my $temp_output_directory = shift;
   my $path_to_bedtool = shift;
   my $path_to_blacklist = shift;
   my $type_of_analysis1 = shift;
   my $path_to_germDB = shift;
   
   my $inputFile;
   if($type_of_analysis1 eq "somatic" && defined $path_to_germDB){
    $inputFile = $temp_output_directory.'Analysed_platform1_pileup_2.txt';
   }else{
    $inputFile = $temp_output_directory.'Analysed_platform1_pileup_1.txt';
   }
   my $outputFile = $temp_output_directory.'Analysed_platform1_pileup_3.txt';
   
   # Bedtools function to remove the intersection between 2 bed files (BlackList)
   `$path_to_bedtool intersect -a $inputFile -b $path_to_blacklist -v > $outputFile`;
   `rm $inputFile`;
   
  return();
}   



#============================================
#           File formatting
#============================================

sub Formatting{ 
    
   my $temp_output_directory = shift;
   my $path_to_blacklist = shift;
   my $path_to_germDB = shift;
   my $type_of_analysis1 = shift;
   my $qual_filter = shift;
   my $selected_attributes = @_; my @selected_attributes = @{$_[0]};
   
   my $inputFile;
   if(defined $path_to_blacklist){
    $inputFile = $temp_output_directory.'Analysed_platform1_pileup_3.txt';
   }elsif($type_of_analysis1 eq "somatic" && defined $path_to_germDB && ! defined $path_to_blacklist){
    $inputFile = $temp_output_directory.'Analysed_platform1_pileup_2.txt';
   }else{
    $inputFile = $temp_output_directory.'Analysed_platform1_pileup_1.txt';
   }
   my $outputFile = $temp_output_directory.'Analysed_platform1_pileup_4.txt';
   
   open (OUTPUTFILE, ">$outputFile") or die "Cannot open file $outputFile: $!";
   open (INPUTFILE, "<$inputFile") or die "Cannot open file $inputFile: $!";
   while (<INPUTFILE>){
    chomp;
    my @l = split /[\t,]+/;  
    my $data = $l[10];
    for(my $i = 11;$i <= @selected_attributes+9; $i++){$data = $data.",".$l[$i]};
    if($qual_filter =~ /on/){
      print (OUTPUTFILE "$l[3]\t$l[0]\t$l[1]\t$l[2]\t$l[4]\t$l[5]\t$l[8]\t$l[9]\t$data\n");
    }else{
      print (OUTPUTFILE "$l[3]\t$l[0]\t$l[1]\t$l[2]\t$l[4]\t$l[5]\t$l[6]\t$l[7]\t$data\n");
    }
   }
   close INPUTFILE; close OUTPUTFILE;
   `rm $inputFile`;
   
  return();
}



#============================================
#         Germline coverage filter
#============================================

sub GermlineFilter{  

  my $input_directory = shift;
  my $temp_output_directory = shift;
  my $coverage_filter_N = shift;
  my $covered_filter_N = shift;
  my $inputFile1 = $temp_output_directory.'Analysed_platform1_pileup_4.txt';
  my $outputFile1 = $temp_output_directory.'Filtered_platform1_N.txt';
  my ($outputFile2, %hash); 
  
  open (INPUTFILE1, "<$inputFile1") or die "Cannot open file $inputFile1: $!";
  while (my $l =<INPUTFILE1>){
    chomp $l;
    my @l = split(/\t/,$l);
    $hash{$l[0]}{$l[1]."\t".$l[3]}=$l; 
  }
  close INPUTFILE1;
  
  open (OUTPUTFILE1, ">>$outputFile1") or die "Cannot open file $outputFile1: $!";
  if($covered_filter_N eq "on"){
    $outputFile2 = $temp_output_directory.'Analysed_platform1_pileup_5.txt';
    open (OUTPUTFILE2, ">>$outputFile2") or die "Cannot open file $outputFile2: $!";
  }
  opendir(DIR, $input_directory) or die "cannot open directory $input_directory";
  my @files = readdir DIR;
  foreach my $inputFile2(@files){
    if($inputFile2 =~ /Platform1_N_(\w+).pu/){
      my $sample = $1;
      my $path_to_input = $input_directory.$inputFile2;
      open (INPUTFILE2, "<$path_to_input") or die "Cannot open file $path_to_input: $!";
      while (my $l =<INPUTFILE2>){
	chomp $l;
	my @l = split(/\t/,$l);
	if(exists $hash{$sample}{$l[0]."\t".$l[1]}){
	  if($l[3] >= $coverage_filter_N){
	    if($covered_filter_N eq "on"){
	      print (OUTPUTFILE2 $hash{$sample}{$l[0]."\t".$l[1]}."\n");
	    }
	    print (OUTPUTFILE1 $sample."\t".$l."\n"); 
	  }
	}
      }
    }
  }  
  close INPUTFILE2; close OUTPUTFILE1; close OUTPUTFILE2; close DIR;

  return();
}



#============================================
#          Pileup partial analysis
#============================================

sub PileupSNPPartialAnalysis{

    #variables declaration
    my $temp_output_directory = shift;
    my $bqv = shift;
    my $mqv = shift;
    my $qual_filter = shift;
    my $nbvar_N = shift;
    my $inputFile = $temp_output_directory.'Filtered_platform1_N.txt';
    my $outputFile = $temp_output_directory.'Prepared_platform1_N.txt';
    
    open (OUTPUTFILE, ">$outputFile") or die "Cannot open file $outputFile: $!";
    open (INPUTFILE, "<$inputFile") or die "Cannot open file $inputFile: $!";
 
    while (<INPUTFILE>){
      chomp;
      my @l = split(/\t/);
      
      #filter of special pileup characters
      $l[5] =~ s/\^.//g;
      $l[5] =~ tr/\!//d;
      $l[5] =~ tr/\$//d;
      $l[5] =~ tr/,/./;
      $l[3] = uc($l[3]);
      $l[5] = uc($l[5]);
      
      #filter of indels
	#variables def     
	my @motif_untrimmed;
	my @motif;
	my @motif_size;
	#get motif
	@motif_untrimmed = ($l[5] =~ /[+|-]\d{1,2}[A|T|G|C|N]+/g);
	foreach my $m(@motif_untrimmed){
	  if($m =~ /(\d{1,2})([A|T|G|C|N]+)/){
	    my $motif_size = $1;
	    my $motif_untrimmed = $2;
	    my $motif_trimmed = substr($motif_untrimmed,0,$motif_size);
	    push(@motif, $motif_trimmed);
	  }
	}
	#suppression of the motif
	foreach my $m(@motif){
	  $l[5] =~ s/[+|-]\d{1,2}$m//;
	}
      
      my @vartot; 
      $vartot[0] = $l[5] =~ tr/A//;
      $vartot[1] = $l[5] =~ tr/T//;
      $vartot[2] = $l[5] =~ tr/G//;
      $vartot[3] = $l[5] =~ tr/C//;
      
      my @N;
      $N[0]="A"; $N[1]="T"; $N[2]="G"; $N[3]="C";
      for (my $i=0;$i<4;$i++){
	if($vartot[$i] != 0){
	  #variables declaration
	  my $varhighquality = 0;
	  my $refhighquality = 0;
	  my $highqualcov;
	  my $offset = 0;
	  my @readquality =();
	  my @basequality =(); 
      
	  #Quality filter
	  my $refpos = index($l[5], ".", $offset);
	  my $varpos = index($l[5], "$N[$i]", $offset);
	  @basequality = split(//,$l[6]);
	  @basequality = map(ord $_, @basequality);
	  @readquality = split(//,$l[7]);
	  @readquality = map(ord $_, @readquality);
			      
	  while ($varpos != -1) {
	      # High quality var count(high qual var read + high qual base var) 	
	      if($basequality[$varpos]>=$bqv && $readquality[$varpos]>=$mqv){
		$varhighquality ++;
	      }
	      $offset = $varpos + 1;
	      $varpos = index($l[5], "$N[$i]", $offset);
	  }
          
	  while ($refpos != -1) {;
	      #High quality ref count(high qual ref read + high qual base ref) 	
	      if($basequality[$refpos]>=$bqv && $readquality[$refpos]>=$mqv){
		$refhighquality ++;
	      }
	      $offset = $refpos + 1;
	      $refpos = index($l[5], ".", $offset);
	  }
	  
	  #High qual coverage 
	  $highqualcov = $varhighquality + $refhighquality;
	  
     	  #FILTER 2
     	  my $start = $l[2]-1; my $end = $l[2];
	  if($qual_filter =~ /on/ && $varhighquality >= $nbvar_N){
	      print (OUTPUTFILE "$l[0]\t$l[1]\t$start\t$end\t$l[3]\t$N[$i]\t$highqualcov\t$varhighquality\n");
	  }elsif($vartot[$i] >= $nbvar_N){
	      print (OUTPUTFILE "$l[0]\t$l[1]\t$start\t$end\t$l[3]\t$N[$i]\t$l[4]\t$vartot[$i]\n");
	  }
	  
	}
      }
    }
    close INPUTFILE; close OUTPUTFILE;
  
  return();
} 


sub PileupIndelPartialAnalysis{

    my $temp_output_directory = shift;
    my $bqv = shift;
    my $mqv = shift;
    my $qual_filter = shift;
    my $nbvar_N = shift;
    my $inputFile = $temp_output_directory.'Filtered_platform1_N.txt';
    my $outputFile = $temp_output_directory.'Prepared_platform1_N.txt';
    
    open (OUTPUTFILE, ">$outputFile") or die "Cannot open file $outputFile: $!";
    open (INPUTFILE, "<$inputFile") or die "Cannot open file $inputFile: $!";
    while (<INPUTFILE>){
	      chomp;
	      my @l = split(/\t/);
	      
	      #filter of special pileup characters
	      $l[5] =~ s/\^.//g;
	      $l[5] =~ tr/\!//d;
	      $l[5] =~ tr/\$//d;
	      $l[5] =~ tr/,/./;
	      $l[5] =~ tr/*/./;
	      $l[3] = uc($l[3]);
	      $l[5] = uc($l[5]);
	      
		# INDELs analysis
		if($l[5] =~ /\+/ || $l[5] =~ /\-/){
			#variables def     
			my @inspos;
			my @delpos;
			my @motif_untrimmed;
			my @motif;
			my @motif_size;
			my $motif;
			my $offset = 0;
			
			#filter of positions with insertion and deletion
			my $countins= $l[5] =~ tr/\+//;
			my $countdel= $l[5] =~ tr/\-//;
			next if($countins != 0 && $countdel != 0);
			
			#filter of indels containing undifined bases (N) + forward and reverse info
			@motif_untrimmed = ($l[5] =~ /[+|-]\d{1,2}[A|T|G|C|N]+/g);
			foreach my $m(@motif_untrimmed){
			  if($m =~ /(\d{1,2})([A|T|G|C|N]+)/){
			    my $motif_size = $1;
			    push(@motif_size, $motif_size);
			    my $motif_untrimmed = $2;
			    push(@motif_untrimmed, $motif_untrimmed);
			    if(substr($motif_untrimmed,0,$motif_size) =~ /N/){
			      my $motif = $motif_size.substr($motif_untrimmed,0,$motif_size);
			      #undefined suppression
			      $l[5] =~ s/[+|-]$motif//;
			    }elsif(substr($motif_untrimmed,0,$motif_size) =~ /[A|T|G|C]/){
			      push(@motif, substr($motif_untrimmed,0,$motif_size));
			    }
			  }
			}
						
			#get motif
			next if scalar(@motif) == 0;
			if(scalar(@motif) != 0){
			  my %counts;
			  ++$counts{$_} for @motif;
			  $motif = (reverse sort { $counts{$a} <=> $counts{$b} } keys %counts)[0]; 
			  if(keys %{{ map {$_, 1} @motif }} != 1){
			    my $second = (reverse sort { $counts{$a} <=> $counts{$b} } keys %counts)[1];
			    next if $counts{$motif} == $counts{$second};
			  }
			}
			  
			#motif size
			my $motif_size = length($motif);
			my $start = $l[2]-1;
			my $end = $start + $motif_size;
			
			#replacement of the character before the motif to track it
			if($countins != 0){
			  my $inspos = index(uc $l[5],"+", $offset);
			  while ($inspos != -1) {
			    push (@inspos, $inspos);
			    $offset = $inspos + 1;
			    $inspos = index(uc $l[5],"+", $offset);
			  }
			  foreach $inspos(@inspos){
			    my $fragment = substr($l[5],($inspos-1),1,'X');
			  }    
			}elsif($countdel != 0){
			  my $delpos = index(uc $l[5],"-", $offset);
			  while ($delpos != -1) {
			    push (@delpos, $delpos);
			    $offset = $delpos + 1;
			    $delpos = index(uc $l[5],"-", $offset);
			  }
			  foreach $delpos(@delpos){
			    my $fragment = substr($l[5],($delpos-1),1,'X');
			  }
			}
			
			#suppression of the motif
			foreach my $m(@motif){
			  $l[5] =~ s/[+|-]\d{1,2}$m//;
			}
			
			#ref count
			my $reftot = $l[5] =~ tr/\.//;
			#var count
			my $vartot = $l[5] =~ tr/X//;

			#analysis of the base preceding the indel 
			my $refhighquality = 0;
			my $varhighquality = 0;
			my $highqualcov;
			my @readquality =();
			my @basequality =(); 

			#base analysis
			$offset = 0;
			my $refpos = index($l[5], ".", $offset);
			my $varpos = index($l[5], "X", $offset);
			@basequality = split(//,$l[6]);
			@basequality = map(ord $_, @basequality);
			@readquality = split(//,$l[7]);
			@readquality = map(ord $_, @readquality);
			while ($refpos != -1) {
				# High quality ref count(high qual ref read + high qual base ref) 
				if($basequality[$refpos]>=$bqv && $readquality[$refpos]>=$mqv){
					$refhighquality ++;
				}
				$offset = $refpos + 1;
				$refpos = index($l[5], ".", $offset);
			}
			while ($varpos != -1) {
				# High quality var count(high qual var read + high qual base var) 	
				if($basequality[$varpos]>=$bqv && $readquality[$varpos]>=$mqv){
					$varhighquality ++;
				}
				$offset = $varpos + 1;
				$varpos = index($l[5], "X", $offset);
			}
						
			#High qual coverage evaluation 
			$highqualcov = $varhighquality + $refhighquality;
			
		
		
			#FILTER 2
			if($countins != 0){
			  if($qual_filter =~ /on/ && $varhighquality >= $nbvar_N){
			    print (OUTPUTFILE "$l[0]\t$l[1]\t$start\t$l[2]\t-\t$motif\t$highqualcov\t$varhighquality\n");
			  }elsif($vartot >= $nbvar_N){
			    print (OUTPUTFILE "$l[0]\t$l[1]\t$start\t$l[2]\t-\t$motif\t$l[4]\t$vartot\n");
			  }
			}elsif($countdel != 0){
			  if($qual_filter =~ /on/ && $varhighquality >= $nbvar_N){
			    print (OUTPUTFILE "$l[0]\t$l[1]\t$start\t$end\t$motif\t-\t$highqualcov\t$varhighquality\n");
			  }elsif($vartot >= $nbvar_N){
			    print (OUTPUTFILE "$l[0]\t$l[1]\t$start\t$end\t$motif\t-\t$l[4]\t$vartot\n");
			  }
			}
			
		}
    }
    close INPUTFILE; close OUTPUTFILE;
  
  return();
} 




#============================================
#           Platform2 vcf parsing
#============================================

sub Plat2parsing{
  
  #variables declaration
  my $input_directory = shift;
  my $inputFile = shift;
  my $temp_output_directory = shift;
  my $sample_id = shift;
  my $type_of_analysis3 = shift;
  my $outputFile = $temp_output_directory.'Prepared_platform2.txt';
  my $path_to_input = $input_directory.$inputFile;
  my $start; my $end;
  
  open (OUTPUTFILE, ">>$outputFile") or die "Cannot open file $outputFile: $!";
  open (INPUTFILE, "<$path_to_input") or die "Cannot open file $path_to_input: $!";
  
  while (<INPUTFILE>){
    chomp;
    my @l=split(/\t/);
    next if $l[0] =~ /^#/;
    if($type_of_analysis3 eq "SNP" && length($l[3]) == length($l[4])){
      print (OUTPUTFILE "$sample_id\t$l[0]\t".($l[1]-1)."\t$l[1]\t$l[3]\t$l[4]\n");
    }elsif($type_of_analysis3 eq "Indel" && length($l[3]) < length($l[4])){
      print (OUTPUTFILE "$sample_id\t$l[0]\t".($l[1]-1)."\t$l[1]\t-\t".substr($l[4],1)."\n");
    }elsif($type_of_analysis3 eq "Indel" && length($l[3]) > length($l[4])){
      print (OUTPUTFILE "$sample_id\t$l[0]\t".($l[1]-1)."\t".(($l[1]-1) + length(substr($l[3],1)))."\t".substr($l[3],1)."\t-\n");
    }
  }
  close INPUTFILE; close OUTPUTFILE;
  
  return();
}



#============================================
#       Somatic-specificity analysis
#============================================

sub SomaticSpecificAnalysis{

  #variables declaration
  my $temp_output_directory = shift;
  my $somatic_pvalue = shift;
  my $covered_filter_N = shift;
  my $outputFile_platform1_Tspec = $temp_output_directory.'Analysed_platform1_pileup_Tspec.txt';
  my $inputFile_N = $temp_output_directory.'Prepared_platform1_N.txt';
  my $inputFile_T;
  
  if($covered_filter_N eq "on"){
    $inputFile_T = $temp_output_directory.'Analysed_platform1_pileup_5.txt';
  }else{
    $inputFile_T = $temp_output_directory.'Analysed_platform1_pileup_4.txt';
  }
  
  my $errorCode;
  my (%platform1_T, %platform1_N); 

  open (INPUTFILE_PLATFORM1_T, "<$inputFile_T") or die "Cannot open file $inputFile_T: $!";
  open (INPUTFILE_PLATFORM1_N, "<$inputFile_N") or die "Cannot open file $inputFile_N: $!";
  
  while (<INPUTFILE_PLATFORM1_T>){
    chomp;
    my @l=split(/\t/);
    my @value = ($l[6],$l[7],$l[8]);
    $platform1_T{$l[0]."\t".$l[1]."\t".$l[2]."\t".$l[3]."\t".$l[4]."\t".$l[5]}=[ @value ];
  } 
  close INPUTFILE_PLATFORM1_T;
  
  while (<INPUTFILE_PLATFORM1_N>){
    chomp;
    my @l=split(/\t/);
    my @value = ($l[6],$l[7]);
    $platform1_N{$l[0]."\t".$l[1]."\t".$l[2]."\t".$l[3]."\t".$l[4]."\t".$l[5]}=[ @value ];
  }
  close INPUTFILE_PLATFORM1_N;
  
  open (OUTPUTFILE_PLATFORM1_TSPEC, ">>$outputFile_platform1_Tspec") or die "Cannot open file $outputFile_platform1_Tspec: $!";
  foreach my $keys(keys %platform1_T){  
    if(exists $platform1_N{$keys}){
      ###Somatic Fisher test
      #n1p = total alleles A+B in N
      #np1 = total of allele A in N and T
      #n11 = total of allela A in N
      #npp = total of all alleles A+B in N + A+B in T
      my $errorCode_soma;
      my $n1p = $platform1_N{$keys}->[0];
      my $np1 = (${$platform1_N{$keys}}[0] - ${$platform1_N{$keys}}[1]) + (${$platform1_T{$keys}}[0] - ${$platform1_T{$keys}}[1]);
      my $n11 = ${$platform1_N{$keys}}[0] - ${$platform1_N{$keys}}[1];
      my $npp = ${$platform1_T{$keys}}[0] + ${$platform1_N{$keys}}[0];
      
      my $right_value_soma = sprintf '%1.4f', calculateStatistic( n11=>$n11,
								  n1p=>$n1p,
								  np1=>$np1,
								  npp=>$npp );
      if( ($errorCode_soma = getErrorCode())){
	print $errorCode_soma." - ".getErrorMessage();
      }elsif($right_value_soma <= $somatic_pvalue){
	print (OUTPUTFILE_PLATFORM1_TSPEC "$keys"."\t".${$platform1_T{$keys}}[0]."\t".${$platform1_T{$keys}}[1]."\t".$right_value_soma."\t".${$platform1_T{$keys}}[2]."\n");
      }
    }else{
      print (OUTPUTFILE_PLATFORM1_TSPEC "$keys"."\t".${$platform1_T{$keys}}[0]."\t".${$platform1_T{$keys}}[1]."\t0\t".${$platform1_T{$keys}}[2]."\n");
    }
  }
  close OUTPUTFILE_PLATFORM1_TSPEC; 
  %platform1_T=(); %platform1_N=();
  
  print "Somatic-specific analysis is completed\n";	
  
  return();
}



#============================================
#          .arff format creation
#============================================

sub PrintArffHeadersInFile{
    
  #variables declaration
  my $outputFile = shift;
  my $selected_attributes = @_; my @selected_attributes = @{$_[0]};
 
  open (PLATFORM1_ARFF, ">$outputFile") or die "Cannot open file $outputFile: $!";
  print PLATFORM1_ARFF '@RELATION SNooPer'."\n\n";
  foreach my $attributes(@selected_attributes){
    print PLATFORM1_ARFF '@ATTRIBUTE '.$attributes.' numeric'."\n";   
  }
  print PLATFORM1_ARFF '@ATTRIBUTE class {0,1}'."\n\n".'@DATA'."\n";
  close PLATFORM1_ARFF;
    
  return();
}


sub ArffCreationTrain{
	
	#variables declaration
	my $inputFile_platform1 = shift;
	my $inputFile_platform2 = shift;
	my $outputFile_platform1_train_arff = shift;
	my $nbvalidated = shift;
	my $nbnonvalidated = shift;
	my $validated_variant_fraction = shift;
	my $validated_nonvalidated_ratio = shift;
	my (%platform1, %platform2, %ValidePos_1, %NonValidePos_1);
	
	#Read AnalysedPlatform1_File
	my $CountPosValAndNotVal = 0;
	open (PLATFORM1_INPUT, "<$inputFile_platform1") or die "Cannot open file $inputFile_platform1: $!";
	while (<PLATFORM1_INPUT>){
		chomp;
		my @l=split(/\t/);
		$CountPosValAndNotVal++;
		$platform1{$l[0]."\t".$l[1]."\t".$l[2]."\t".$l[3]."\t".$l[4]."\t".$l[5]}=$l[9];
	}
	close PLATFORM1_INPUT;

	#Read AnalysedPlatform2_File
	open (PLATFORM2_INPUT, "<$inputFile_platform2") or die "Cannot open file $inputFile_platform2: $!";
	while (<PLATFORM2_INPUT>){
		chomp;
		my @l=split(/\t/);
		$platform2{$l[0]."\t".$l[1]."\t".$l[2]."\t".$l[3]."\t".$l[4]."\t".$l[5]}=1;
	}
	close PLATFORM2_INPUT;


	#identify shared positions between PLATFORM1 & PLATFORM2
	my $CountPosVal = 0;
	foreach my $keys(keys %platform1){
		if(exists $platform2{$keys}){
		  $CountPosVal++;
		  $ValidePos_1{$CountPosVal}=$platform1{$keys};
		  delete $platform1{$keys};
		}
	}

	#test $CountPosVal
	if($CountPosVal == 0){
		print "Error, no shared position between platform1 and platform2, no TP provided to train the model\n";
		exit 0;
	}
	if($CountPosVal == $CountPosValAndNotVal){
		print "Error, all positions are shared between platform1 and platform2, no FP provided to train the model\n";
		exit 0;
	}

	#define the number of valid positions that have to be selected randomly for the training file
	my $SelectRandomCount;
	if($nbvalidated == 0){
	  $SelectRandomCount = $CountPosVal*$validated_variant_fraction;
	}else{ 
	  $SelectRandomCount = $nbvalidated;
	}  

	#define train_arff_File with valid positions
	open (ARFF_TRAIN, ">>$outputFile_platform1_train_arff") or die "Cannot open file $outputFile_platform1_train_arff: $!";
	my $range_val = $CountPosVal +1;
	for (my $count_Select_random = 1; $count_Select_random <= $SelectRandomCount; $count_Select_random++){
		my $random_number = int(rand($range_val));

		while (!exists $ValidePos_1{$random_number}){
			$random_number = int(rand($range_val));
		}
		print (ARFF_TRAIN $ValidePos_1{$random_number}.",1\n");
		delete $ValidePos_1{$random_number};
	}
	%ValidePos_1=();
	
	#prepare hash of non valid positions for random selection
	my $CountPosNonVal = 0;
	foreach my $keys(keys %platform1){
		$CountPosNonVal++;
		$NonValidePos_1{$CountPosNonVal}=$platform1{$keys};
		delete $platform1{$keys};
	}


	#define the number of non valid positions that have to be selected randomly (training file)
	if($nbnonvalidated == 0){
	  if(($CountPosVal/$CountPosNonVal) < $validated_nonvalidated_ratio){
		  $SelectRandomCount = ($CountPosVal*$validated_variant_fraction) / $validated_nonvalidated_ratio;
		  print ("Warning, the count of non valid position have been limited to $SelectRandomCount in the training file to respect the ratio of 1/$validated_nonvalidated_ratio \n"); 
	  }else{
		  $SelectRandomCount = ($CountPosVal*$validated_variant_fraction) / ($CountPosVal/$CountPosNonVal);
	  }
	}else{  
	  $SelectRandomCount = $nbnonvalidated; 
	}
	
	#add non valid position to train_arff_File
	my $range_nonval = $CountPosNonVal +1;
	for (my $count_Select_random = 1; $count_Select_random <= $SelectRandomCount; $count_Select_random++){
		my $random_number = int(rand($range_nonval));
		while (!exists $NonValidePos_1{$random_number}){
			$random_number = int(rand($range_nonval));
		}
		print (ARFF_TRAIN $NonValidePos_1{$random_number}.",0\n");
		delete $NonValidePos_1{$random_number};
	}
	close ARFF_TRAIN;
	%NonValidePos_1=();
	print "The .arff file for training is completed\n";
	
	
	return ();
}  


sub ArffCreationEval{
  
  #variables declaration
  my $temp_output_directory = shift;
  my $inputFile_platform1 = shift;
  my $inputFile_platform2 = shift;
  my $outputFile_platform1_arff = shift;
  my (%platform1, %platform2);

  open (PLATFORM1_INPUT, "<$inputFile_platform1") or die "Cannot open file $inputFile_platform1: $!";
  open (PLATFORM1_INPUT, "<$inputFile_platform2") or die "Cannot open file $inputFile_platform2: $!";
  open (PLATFORM1_ARFF, ">>$outputFile_platform1_arff") or die "Cannot open file $outputFile_platform1_arff: $!";
  
  #Read AnalysedPlatform1_File
  open (PLATFORM1_INPUT, "<$inputFile_platform1") or die "Cannot open file $inputFile_platform1: $!";
  while (<PLATFORM1_INPUT>){
    chomp;
    my @l=split(/\t/);
    $platform1{$l[0]."\t".$l[1]."\t".$l[2]."\t".$l[3]."\t".$l[4]."\t".$l[5]}=$l[9];
  }
  close PLATFORM1_INPUT;

  #Read AnalysedPlatform2_File
  open (PLATFORM2_INPUT, "<$inputFile_platform2") or die "Cannot open file $inputFile_platform2: $!";
  while (<PLATFORM2_INPUT>){
    chomp;
    my @l=split(/\t/);
    $platform2{$l[0]."\t".$l[1]."\t".$l[2]."\t".$l[3]."\t".$l[4]."\t".$l[5]}=1;
  }
  close PLATFORM2_INPUT;

  #identify shared positions between PLATFORM1 & PLATFORM2
  foreach my $keys(keys %platform1){
    if(exists $platform2{$keys}){
      print (PLATFORM1_ARFF $platform1{$keys}.",1\n");
    }else{
      print (PLATFORM1_ARFF $platform1{$keys}.",0\n");
    }
  }
  close PLATFORM1_ARFF;
    
  return();
}
 
 
sub ArffCreation{
  
  #variables declaration
  my $inputFile_platform1 = shift;
  my $outputFile_platform1_arff = shift;
  
  open (PLATFORM1_INPUT, "<$inputFile_platform1") or die "Cannot open file $inputFile_platform1: $!";
  open (PLATFORM1_ARFF, ">>$outputFile_platform1_arff") or die "Cannot open file $outputFile_platform1_arff: $!";
  
  while (<PLATFORM1_INPUT>){
    chomp;
    my @l=split(/\t/);
    print (PLATFORM1_ARFF $l[9].",0\n");
  }
  close PLATFORM1_ARFF; close PLATFORM1_INPUT;
  
  return();
}
 

 
 
#============================================
#       Training curves construction
#============================================ 

sub CurvesCreation{
  
  my $stat_curve = shift;
  my $temp_output_directory = shift;
  my $output_directory = shift;
  my $stat_curve_R = $temp_output_directory.'Stat_curve.txt';
  my $stat_curve_pdf = $output_directory.'Training_curves.pdf';
  my $R = Statistics::R->new();
  
  open (STAT, "<$stat_curve") or die "Cannot open file $stat_curve: $!";
  open (STAT_R, ">$stat_curve_R") or die "Cannot open file $stat_curve_R: $!";    
  while (my $l =<STAT>){
    next if($l !~ /^\d/);
    print (STAT_R $l);
  }
  
  $R->run('pdf("'.$stat_curve_pdf.'");');
  
  $R->run('file<-read.table("'.$stat_curve_R.'", sep = ",", header = F);');
  $R->run('x <- c(0.0,0.2,0.4,0.6,0.8,1.0);');
  $R->run('y <- c(0.0,0.2,0.4,0.6,0.8,1.0);');
  
  #ROC curve
  $R->run('plot(y ~ x, xaxt="n", ylim = c(0,1), main = "ROC curve", pch = "", xlab="False positive rate", ylab="True positive rate");');
  $R->run('abline(0, 1, col="darkgrey");');
  $R->run('FPR<-file[[5]];');
  $R->run('TPR<-file[[6]];');
  $R->run('FPR_sorted<-sort(FPR);');
  $R->run('TPR_sorted<-sort(TPR);');
  $R->run('lines(FPR, TPR, type="o", col="black", lty=1, pch = 21, bg = "darkcyan");');
  $R->run('axis(1,at=x, labels=c("0.0","0.2","0.4","0.6","0.8","1.0"));');
  $R->run('require(pracma);');
  $R->run('AUC_temp<-trapz(FPR_sorted, TPR_sorted);');
  $R->run('AUC<-signif(AUC_temp, digits=4);');
  $R->run('legend(0.6,0.1, paste("AUC =", AUC), cex=1.2, bty="n");');
  
  #Precision Recall curve
  $R->run('plot(y ~ x, xaxt="n", ylim = c(0,1), main = "Precision vs Recall curve", pch = "", xlab="Recall", ylab="Precision");');
  $R->run('abline(0, 1, col="darkgrey");');
  $R->run('Recall<-file[[8]];');
  $R->run('Precision<-file[[7]];');
  $R->run('Recall_sorted<-sort(Recall);');
  $R->run('Precision_sorted<-sort(Precision);');
  $R->run('lines(Recall, Precision, type="o", col="black", lty=1, pch = 21, bg = "blue");');
  $R->run('axis(1,at=x, labels=c("0.0","0.2","0.4","0.6","0.8","1.0"));');
  $R->run('require(pracma);');
  $R->run('AUC_temp<-trapz(Recall_sorted, Precision_sorted);');
  $R->run('AUC<-signif(AUC_temp, digits=4);');
  $R->run('legend(0.6,0.1, legend= paste("AUC =", AUC), cex=1.2, bty="n");');
  
  $R->run('dev.off();');
  $R->stopR();
  
  return();
}
 
 
 
  
#============================================
#        Evaluation of the algorithm
#============================================

sub EvalStat{
  
  #variables declaration
  my $eval_classification = shift;
  my $eval_classification_stat = shift;
  my $TN=0; my $FP=0; my $TP=0; my $FN=0;
  my $TN2=0; my $FP2=0; my $TP2=0; my $FN2=0;
  
  # Confusion Matrix
  open (OUTPUTFILE_PLATFORM1_EVAL, "<$eval_classification") or die "Cannot open file $eval_classification: $!";
  while (my $l =<OUTPUTFILE_PLATFORM1_EVAL>){
    chomp $l;
    next if $l =~ /=/;
    next if $l =~ /#/;
    next if $l eq "";
    $l =~ s/\+//g;
    $l =~ s/\h+/\t/g;
    $l =~ s/^\t+//;
    my @l=split(/\t/,$l);
  
    if($l[1] eq "1:0" && $l[2] eq "2:1" && $l[3] >= 0.9){
      $FP++;
    }elsif($l[1] eq "2:1" && $l[2] eq "2:1" && $l[3] >= 0.9){
      $TP++;
    }
    
    if($l[1] eq "1:0"){
      if($l[2] eq "2:1"){
	$FP2++;
      }elsif($l[2] eq "1:0"){
	$TN2++;
      }
    }elsif($l[1] eq "2:1"){
      if($l[2] eq "2:1"){
	$TP2++;
      }elsif($l[2] eq "1:0"){
	$FN2++;
      }
    }
    
  }
  close OUTPUTFILE_PLATFORM1_EVAL;
  
  # Kappa calculation
  my $tot = $TP+$TN2+$FP+$FN2;
  my $pre = (($TN2+$FP)/$tot)*(($TN2+$FN2)/$tot)*(($FN2+$TP)/$tot)*(($FP+$TP)/$tot);
  my $pra = ($TN2+$TP)/$tot;
  my $kappa = ($pra-$pre)/(1-$pre);
  
  my $tot2 = $TP2+$TN2+$FP2+$FN2;
  my $pre2 = (($TN2+$FP2)/$tot2)*(($TN2+$FN2)/$tot2)*(($FN2+$TP2)/$tot2)*(($FP2+$TP2)/$tot2);
  my $pra2 = ($TN2+$TP2)/$tot2;
  my $kappa2 = ($pra2-$pre2)/(1-$pre2);
  
  # Output
  open (OUTPUTFILE_PLATFORM1_EVAL_STAT, ">$eval_classification_stat") or die "Cannot open file $eval_classification_stat: $!";
  print(OUTPUTFILE_PLATFORM1_EVAL_STAT "
  These results give an evaluation of the model's performances of classification.
  ---------------------------------------------------------------------------------------------------------------\n
  ####################
  Algorithm evaluation
  ####################\n
  Only class probability >= 0.9 are considered (recommended)
  ---------------------------------------------------------------------------------------------------------------
  TP= ".$TP."
  TN= ".$TN2."
  FP= ".$FP."
  FN= ".$FN2."
  Precision = ".$TP/($TP+$FP)."
  Recall = ".$TP/($TP+$FN2)."
  Kappa = ".$kappa."\n\n
  ####################
  Algorithm evaluation
  ####################\n
  All positions matching the quality filters are considered (no class probability filter)
  ---------------------------------------------------------------------------------------------------------------
  TP= ".$TP2."
  TN= ".$TN2."
  FP= ".$FP2."
  FN= ".$FN2."
  Precision = ".$TP2/($TP2+$FP2)."
  Recall = ".$TP2/($TP2+$FN2)."
  Kappa = ".$kappa2."\n"
  );
  
  close OUTPUTFILE_PLATFORM1_EVAL_STAT;
  print "Algorithm evaluation is completed\n";
  
  return();
}




#============================================
#         Final final construction
#============================================

sub FinalFileCreation{

  #variables declaration
  my $final_classification = shift;
  my $outputFile_platform1 = shift;
  my $raw_classification = shift;
  my $type_of_analysis1 = shift;
  my $type_of_analysis3 = shift;
  my %input; my %raw; my $n = 0; my $n2 = 0;
  
  open (OUTPUTFILE_PLATFORM1_FINAL, ">>$final_classification") or die "Cannot open file $final_classification: $!";
  
  open (OUTPUTFILE_PLATFORM1_RAW, "<$raw_classification") or die "Cannot open file $raw_classification: $!";
      while (my $l =<OUTPUTFILE_PLATFORM1_RAW>){
	chomp $l;
	next if $l =~ /=/;
	next if $l =~ /#/;
	next if $l eq "";
	$n++;
	$l =~ s/\+//g;
	$l =~ s/\h+/\t/g;
	$l =~ s/^\t+//;
	my @l=split(/\t/,$l);
	$l[2] =~ s/2:1/PASS/g;
	$l[2] =~ s/1:0/0/g;
	$raw{$n}=$l[2]."\t".$l[3];  
      } 
      close OUTPUTFILE_PLATFORM1_RAW;
	
   open (OUTPUTFILE_PLATFORM1_INPUT, "<$outputFile_platform1") or die "Cannot open file $outputFile_platform1: $!";
   
   print (OUTPUTFILE_PLATFORM1_FINAL "Chromosome\tPosition start\tPosition end\tReference allele\tVariant allele\tSampleId\tCoverage\tRaw allelic frequency\tSomatic P-Value\tPrediction\tClass probability (probability that the instance actually belongs to the determined class)\n");
      while (my $l =<OUTPUTFILE_PLATFORM1_INPUT>){
	chomp $l;
	my @l = split /[\s+,]+/, $l;
	$n2++;
	$l[1] =~ tr/chr//d;
	my $start_indel = $l[2]+1;
	my $allelic_freq_raw = sprintf '%1.4f', ($l[7]/$l[6]);
	
	#Annovar input format
	if($type_of_analysis1 eq "somatic"){
	  if($type_of_analysis3 eq "SNP"){
	    $input{$n2}=$l[1]."\t".$l[3]."\t".$l[3]."\t".$l[4]."\t".$l[5]."\t".$l[0]."\t".$l[6]."\t".$allelic_freq_raw."\t".$l[8];
	  }elsif($type_of_analysis3 eq "Indel"){
	    $input{$n2}=$l[1]."\t".$start_indel."\t".$l[3]."\t".$l[4]."\t".$l[5]."\t".$l[0]."\t".$l[6]."\t".$allelic_freq_raw."\t".$l[8]; 
	  }
	}else{
	  if($type_of_analysis3 eq "SNP"){
	    $input{$n2}=$l[1]."\t".$l[3]."\t".$l[3]."\t".$l[4]."\t".$l[5]."\t".$l[0]."\t".$l[6]."\t".$allelic_freq_raw."\tNA";
	  }elsif($type_of_analysis3 eq "Indel"){
	    $input{$n2}=$l[1]."\t".$start_indel."\t".$l[3]."\t".$l[4]."\t".$l[5]."\t".$l[0]."\t".$l[6]."\t".$allelic_freq_raw."\tNA"; 
	  }
	}
      }
      close OUTPUTFILE_PLATFORM1_INPUT;
      
     
  foreach my $keys(keys %raw){  
    if(exists $input{$keys}){
      print (OUTPUTFILE_PLATFORM1_FINAL $input{$keys}."\t".$raw{$keys}."\n");
    }
  }
  close OUTPUTFILE_PLATFORM1_FINAL; 
  
  
  return();  
}


#============================================
#          Temp files suppression
#============================================

sub TempDirClean{
  
  #variables declaration
  my $temp_output_directory = shift;
  
  `rm -rf $temp_output_directory`;
  
  return();  
}

1;
