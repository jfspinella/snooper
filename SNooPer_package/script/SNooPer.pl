#!/usr/bin/perl 
use strict;
use warnings;

#======================
#  Load Libraries
#======================
use Getopt::Long;
use Pod::Usage;
use lib '/home/jfspinella/Copy/Method_SNPcaller/SNooPer_package/lib/SNooPer/';
#use SNooPer::Lib_SNooPer;
use Lib_SNooPer;


#=========================================================
#                   Reading options
#=========================================================

my $algorithm = "trees.RandomForest";
my ($help, $man, $path, $attributes_selection, $bqv, $contamination, $cost_matrix, $coverage_filter_N, $coverage_filter_T, $covered_filter_N, $cross_validation, $freqinf, $freqsup, $indel_filter, $input_directory, $job_id, $mqv, $nbnonvalidated, $nbvalidated, $nbvar_N, $nbvar_T, $memory, $output_directory, $path_to_bedtool, $path_to_blacklist, $path_to_germDB, $path_to_model_directory, $path_to_weka, $platform1, $qual_filter, $somatic_pvalue, $tr_binom, $tr_fisher, $tree, $type_of_analysis1, $type_of_analysis2, $type_of_analysis3, $validated_nonvalidated_ratio, $validated_variant_fraction);

GetOptions(
	'help|?' => \$help,
	man => \$man,
	"a1=s" => \$type_of_analysis1,
	"a2=s" => \$type_of_analysis2,
	"a3=s" => \$type_of_analysis3,
	"a4=s" => \$attributes_selection,
	"b=s" => \$path_to_bedtool,
	"bqv=i" => \$bqv,
	"c=f" => \$contamination,
	"cf=s" => \$covered_filter_N,
	"cm=s" => \$cost_matrix,
	"cn=i" => \$coverage_filter_N,
	"ct=i" => \$coverage_filter_T,
	"fi=f" => \$freqinf,
	"fs=f" => \$freqsup,
	"g=s" => \$path_to_germDB,
	"i=s" => \$input_directory,
	"id=s" => \$job_id,
	"ind=s" => \$indel_filter,
	"k=i" => \$cross_validation,
	"m=s" => \$path_to_model_directory,
	"mem=s" => \$memory,
	"mqv=i" => \$mqv,
	"nN=i" => \$nbvar_N,
	"nT=i" => \$nbvar_T,
	"nv=i" => \$nbnonvalidated,
	"o=s" => \$output_directory,
	"p1=s" => \$platform1,
	"q=s" => \$qual_filter,
	"r=s" => \$path_to_blacklist,
	"s=f" => \$somatic_pvalue,
	"t=i" => \$tree,
	"v=i" => \$nbvalidated,
	"vf=f" => \$validated_variant_fraction,
	"vr=f" => \$validated_nonvalidated_ratio,
	"w=s" => \$path_to_weka
) or pod2usage(2);
pod2usage(1) if $help;
pod2usage(-verbose => 2) if $man;

__END__

=head1 NAME
 
SNooPer version 0.01
 
=head1 SYNOPSIS

>SNooPer.pl -help [brief help message] -man [full documentation]

>Training: 
>SNooPer.pl -i [input_directory] -o [output_directory] -a1 [type_of_analysis1] -a2 [train] -w [path_to_weka] [options]

>Classify/Evaluate: 
>SNooPer.pl -i [input_directory] -o [output_directory] -a1 [type_of_analysis1] -a2 [classify/evaluate] -m [model] -w [path_to_weka] [options]

=head1 DESCRIPTION

>SNooPer is a highly versatile data mining approach that uses Leo Breiman's Random Forest classification models to accurately call somatic variants in low-depth sequencing data.
>SNooPer needs firstly to be trained using a training set to construct a model that can be then used to call variants on an extended test set.

>The training set has to be constituted of 2 types of files:
>1.pileup files (.pu) presenting equivalent characteristics of the test set on which the trained model will be applied. 
>Somatic analysis format: Platform1_T_sample_id.pu and Platform1_N_sample_id.pu
>Germline analysis format: Platform1_sample_id.pu
>2.vcf files (.vcf) validation files that are ideally orthogonal validations of the positions contained in the pileup files.
>Somatic analysis format: Platform2_T_sample_id.vcf
>Germline analysis format: Platform2_sample_id.vcf

>Each position contained in the pileup files have to be tested so the class (actual variant or error) will be known by comparison with the vcf files.
If a variant is present in the corresponding validation file, it will be considered as an actual variant. If the variant is absent from the validation file, the variant will be considered as an error.
>To be considered as the corresponding validation file of a .pu file, the .vcf file has to present the same sample_id.

=head1 AUTHOR

Jean-Francois Spinella, E<lt>jfspinella@gmail.comE<gt>.
Sainte-Justine UHC Research Center, Montreal University.

=head1 DATE 

March-2015

=head1 REQUIREMENTS

>Weka has to be installed. The current version of SNooPer has been tested with the version weka-3-6-10. (http://sourceforge.net/projects/weka/)

>Bedtools has to be installed if BlackList or germDB_track options are used. The current version of SNooPer has been tested with the version bedtools-2.17.0. (https://code.google.com/p/bedtools/downloads/list)

>During weka development, the BlackList track corresponded to the RepeatMasker track downloaded from UCSC (http://genome.ucsc.edu/cgi-bin/hgTables?command=start). "Assembly" has to be set according to the reference you used to map your sequences, "Group" was set to Variation and Repeats, and "Track" was set to RepeatMasker. The track was downloaded in a .bed format.

>During weka development, the germline database used as germDB_track corresponded to the 1000 Genomes database downloaded from http://www.1000genomes.org/. The track was formated in a .bed format.

=head1 OPTIONS

-help <brief help message>

-man <full documentation>

-a1 <type_of_analysis1> Can take the following values: "somatic" or "germline". "somatic" means that the somatic evaluation will be done based on provided N samples and the eventual provided additional normal data.

-a2 <type_of_analysis2> Can take the following values: "train", "classify" or "evaluate". 
      -> if "train" is selected, a model will be trained based on the comparison of the data generated from 2 different sequencing platforms. A subset of the data provided (subset chosen with the -v and -nv options or automatically selected) for whom the class is known (0/1 = non-validated/validated = not shared by platform1 and 2 / shared by platform1 and 2) will be used for the training. Therefore, a partially overlapping dataset between platform1 and platform2 has to be provided. A final classification of the complete data will be done base on the trained model. Furthermore, an evaluation of the model will be done using a subset of the data never seen by the model. 
      -> if "classify" is selected, the provided dataset is classified using a model created previously. This model has to be in an .arff format (see Weka documentation for more info).
      -> if "evaluate" is selected, the provided dataset is classified using a model created previously. The purpose of this option is an evaluation of an already created model based on the classification of an independant dataset (never used to train the model). To evaluate the model, the class of each variant of the dataset has to been known. Therefore, the data from both platform1 and platform2 have to be provided. These data should be located in a fresh directory containing these files only.

-i <input_directory> Complete path to your input directory.      
      
-o <output_directory> Complete path to your output directory (input and output can be in the same directory).
 
-m <model> Complete path to the directory of an already trained model. This option has to be set only if the type of analysis 2 is "classify" or "evaluate".

-w <path_to_weka> Complete path to Weka software, optimally to weka.jar script.

--------------------------------------------------------
      
-a3 <type_of_analysis3> [optional] Can take the following values: "SNP" or "Indel". The default value is "SNP".

-a4 <attributes_selection> [optional] Can take the following value: "off", "MI" or "BestFirst". The default value is "off". If "MI" is selected (Weka InfoGainAttributeEval + Ranker): evaluation the worth of an attribute by measuring the information gain with respect to the class + ranking of attributes by their individual evaluations. Attributes will be discarded if presenting less than 0.001 bits of mutual information. If "BestFirst" is selected (Weka CfsSubsetEval + BestFirst): evaluation of the worth of a subset of attributes by considering the individual predictive ability of each feature along with the degree of redundancy between them + evaluation of the space of attribute subsets by greedy hillclimbing augmented with a backtracking facility. 

-b <path_to_bedtool> [optional] Complete path to the Bedtools software, optimally to bedtools binary. 

-bqv <bqv> Base quality value (phred) of a variation to be considered as "High Quality". Default value is 20.

-c <contamination> [optional] Fraction of normal cells in the tumoral sample. Can take a value between 0 and 1. Default value is 0.

-cf <covered_filter_N> [optional] Can take the following values: "on" or "off". If the filter is "on", only positions presenting at least a coverage of "coveragefilter_N" in the N will be considered in the T for somatic analysis. Default value is on.

-cm <cost_matrix> [optional] used to adjuste the weight of mistakes on a class (see http://weka.wikispaces.com/CostMatrix). The cost matrix has to be define in a single line format ex: [0.0 5.0; 1.0 0.0], here the weight on false positive is 5 and on false negative is 1.

-cn <coveragefilter_N> [optional] Defines the minimum of coverage for a position to be considered in the N files during a Somatic analysis or the Germline analysis. If a position in the T file doesn't reach the coverage limit in the N file, the position can't be call Somatic and won't be considered. Default value is 8.

-ct <coveragefilter_T> [optional] Defines the minimum of coverage for a position to be considered in the T files during a Somatic analysis. Default value is 8.

-fi <freqinf> [optional] Defines the inferior limit of allele frequency for a variant position to be considered in the T files during a Somatic analysis. Default value is 0.

-fs <freqsup> [optional] Defines the superior limit of allele frequency for a variant position to be considered in the T files during a Somatic analysis. Default value is 1.

-g <path_to_germDB_track> [optional] Complete path to any germline variants database track. This black list usually corresponds to problematic region in the genome. If such a file is provided and if the type_of_analysis1 is "somatic", the variations located at these positions will be considered as germline during the somatic variant calling process. Be careful to provide the track corresponding to the same reference you used to map your sequences.

-id <job_id> [optional] The output file name will be: SNooPer_output_job_id_date.

-ind <indel_filter> [optional] Can take the following values: "on" or "off" when type_of_analysis3 is "SNP". If the filter is "on", pileup lines containing indels won't be considered during the SNP calling process. Default value is on. 

-k <cross_validation> [optional] Integer to define the k-fold cross-validation used to train the model. This option has to be set only if the type of analysis 2 is "train" or "classify". Default value is 10.

-mem <memory> [optional] You can extend the memory available for the virtual machine by setting appropriate options. Ex: -Xmx2g to set it to 2GB. Do not use the -Xms parameter. Using this option, you can also set where the JVM will write temporary files by using the format: -Djava.io.tmpdir=/path/to/tmpdir

-mqv <mqv> [optional] Mapping quality value (phred) of a read presenting a variation to be considered as "High Quality". Default value is 20.

-nN <nbvar_N> [optional] Defines the number of variant for a position to be considered in the N files during a Germline analysis or Somatic analysis.

-nT <nbvar_T> [optional] Defines the number of variant for a position to be considered in the T files during a Somatic analysis.

-nv <nb_of_non_validated_var_to_train> [optional] Number of non-validated variants (disconcordant between platform 1 and 2) used to train your model. If no value is provided, a default value will be calculated from the input file. It prevails over validated_variant_fraction and validated_nonvalidated_ratio.

-p1 <platform1> [optional] Platform used to produce the data to be classified. Can take the following values: "Solid", "Solexa", "Illumina-1.3", "Illumina-1.5" or ">Illumina-1.8". Default value is the Illumina-1.8 or higher ">Illumina-1.8".

-q <qual_filter> [optional] Can take the following values: "on", "on+", "off" or "off". If the filter is "on" or "on+", only variants matching the selected bqv and mqv values will be considered. If "on+" or "off+" are selected, all attributes will be considered including those depending of the quality. Default value is on.

-r <path_to_blacklist> [optional] Complete path to the BlackList track. This black list usually corresponds to problematic region in the genome. If such a file is provided, the variations located in these regions won't be considered during the variant calling process. Be careful to provide the track corresponding to the same reference you used to map your sequences.

-s <somatic_pvalue> [optional] Somatic P-value filter based on a one-tailed Fisher's exact test comparing the somatic and germline allele count. Only variants presenting a P-value <= to this value will be conserved. The default value is 0.1. The value must be between 0 and 1.

-t <tree> [optional] Number of trees to build the model. Default value is 300.

-v <nb_of_validated_var_to_train> [optional] Number of validated variants (concordant between platform 1 and 2) used to train your model. If no value is provided, a default value will be calculated from the input file. It prevails over validated_variant_fraction and validated_nonvalidated_ratio.

-vf <validated_variant_fraction> [optional] Fraction of the validated variants to be used for training. The default value is 1. Note that if the number of validated positions is large, the analysis can be time-consuming.  

-vr <validated_nonvalidated_ratio> [optional] Ratio (nb of non-validated variants / nb of validated variants) in the training dataset. The default value is 0.1. Note that, if the training dataset is extremely imbalanced, a cost sensitive learning can be useful to improve performances. 


=cut  



#====================================================
#                 Options testing
#====================================================

(my $ascii_correction_p1, my $bqv_platform1, $contamination, $coverage_filter_N, $coverage_filter_T, $covered_filter_N, $cross_validation, $freqinf, $freqsup, $indel_filter, $input_directory, $memory, my $mqv_platform1, $nbnonvalidated, $nbvalidated, $nbvar_N, $nbvar_T, $output_directory, $path_to_bedtool, $path_to_model_directory, $path_to_weka, $qual_filter, $somatic_pvalue, my $temp_output_directory, $tree, $type_of_analysis1, $type_of_analysis3, $validated_nonvalidated_ratio, $validated_variant_fraction, my $selected_attributes) = Lib_SNooPer::OptionsTest($algorithm, $attributes_selection, $bqv, $contamination, $cost_matrix, $coverage_filter_N, $coverage_filter_T, $covered_filter_N, $cross_validation, $freqinf, $freqsup, $indel_filter, $input_directory, $job_id, $memory, $mqv, $nbnonvalidated, $nbvalidated, $nbvar_N, $nbvar_T, $output_directory, $path_to_bedtool, $path_to_blacklist, $path_to_germDB, $path_to_model_directory, $path_to_weka, $platform1, $qual_filter, $somatic_pvalue, $tree, $type_of_analysis1, $type_of_analysis2, $type_of_analysis3, $validated_nonvalidated_ratio, $validated_variant_fraction);


#====================================================
#       Output files and variables declaration
#====================================================

my $outputFile_platform1_test_arff = $temp_output_directory.'Analysed_platform1_pileup_test.arff';
my $outputFile_platform1_train_arff = $temp_output_directory.'Analysed_platform1_pileup_training.arff';
my $outputFile_platform1_train_filtered_arff = $temp_output_directory.'Analysed_platform1_pileup_training_filtered.arff';
my $training_stats = $output_directory.'Training_report.txt';
my $eval_raw_classification = $temp_output_directory.$algorithm.'_eval_classification_raw_data.txt';
my $eval_classification_stat = $output_directory.'Evaluation_report.txt';
my $test_raw_classification = $temp_output_directory.$algorithm.'_test_classification_raw_data.txt';
my $final_classification = $output_directory.$algorithm.'_classification_final_data.txt';
my $path_to_model = $path_to_model_directory.$algorithm.'_trained.model';
my $path_to_attributes = $path_to_model_directory.$algorithm.'_trained_model_attributes.txt';
my $stat_curve = $temp_output_directory.'Stat_curve.arff';
my ($generic_inputFile_platform1, $generic_inputFile_platform2, $sample_t1, $sample_n1, $sample_t2, $sample_1, $sample_2);



#====================================================
#           Directory opening and reading
#====================================================

if($type_of_analysis1 eq "somatic"){
  ($sample_t1, $sample_n1, $sample_t2) = Lib_SNooPer::DirRead($input_directory, $type_of_analysis1, $type_of_analysis2);
}elsif($type_of_analysis1 eq "germline"){
  ($sample_1, $sample_2) = Lib_SNooPer::DirRead($input_directory, $type_of_analysis1, $type_of_analysis2);
}


#====================================================
#                 Pileup analysis
#====================================================

if($type_of_analysis1 eq "somatic"){
  
  if($type_of_analysis2 eq "train" || $type_of_analysis2 eq "evaluate"){
   
    # Test match between N and T for somatic
    Lib_SNooPer::SomVsGermTest(\@$sample_t1, \@$sample_n1);
   
    # Test intersection between platform1 and platform2
    my($intersect) =  Lib_SNooPer::InterSectTest(\@$sample_t1, \@$sample_t2);
    
    # Pileup analysis
    foreach my $sample_id(@$sample_t1){
      my $inputFile = "Platform1_T_".$sample_id.".pu";
      if($type_of_analysis3 eq "SNP"){
	Lib_SNooPer::PileupSNPAnalysis($input_directory, $inputFile, $temp_output_directory, $sample_id, $bqv_platform1, $mqv_platform1, $nbvar_T, $coverage_filter_T, $indel_filter, $qual_filter, $freqinf, $freqsup, $contamination, \@$selected_attributes); 
      }elsif($type_of_analysis3 eq "Indel"){
	Lib_SNooPer::PileupIndelAnalysis($input_directory, $inputFile, $temp_output_directory, $sample_id, $bqv_platform1, $mqv_platform1, $nbvar_T, $coverage_filter_T, $qual_filter, $contamination, \@$selected_attributes); 
      }
      print "Pileup analysis for $inputFile is completed\n";
    }
    if(defined $path_to_germDB){
      Lib_SNooPer::FiltergermDB($temp_output_directory, $path_to_bedtool, $path_to_germDB);
    }
    if(defined $path_to_blacklist){
      Lib_SNooPer::BlackList($temp_output_directory, $path_to_bedtool, $path_to_blacklist, $type_of_analysis1, $path_to_germDB);
    }
    Lib_SNooPer::Formatting($temp_output_directory, $path_to_blacklist, $path_to_germDB, $type_of_analysis1, $qual_filter, \@$selected_attributes);
    
    if($coverage_filter_N != 0){
      Lib_SNooPer::GermlineFilter($input_directory, $temp_output_directory, $coverage_filter_N, $covered_filter_N);
    }
    
    # Parsing germline pileup files
    if($type_of_analysis3 eq "SNP"){
      Lib_SNooPer::PileupSNPPartialAnalysis($temp_output_directory, $bqv_platform1, $mqv_platform1, $qual_filter, $nbvar_N); 
    }elsif($type_of_analysis3 eq "Indel"){
      Lib_SNooPer::PileupIndelPartialAnalysis($temp_output_directory, $bqv_platform1, $mqv_platform1, $qual_filter, $nbvar_N); 
    }
    print "Germline pileup analysis is completed\n";
    
    # Parsing platform2 vcf files 
    foreach my $sample_id(@$intersect){ 
      my $inputFile = "Platform2_T_".$sample_id.".vcf";
      Lib_SNooPer::Plat2parsing($input_directory, $inputFile, $temp_output_directory, $sample_id, $type_of_analysis3); 
      print "Data from $inputFile is parsed\n";
    }
    
    # Somatic-specificity analysis
    Lib_SNooPer::SomaticSpecificAnalysis($temp_output_directory, $somatic_pvalue, $covered_filter_N);
    $generic_inputFile_platform1 = $temp_output_directory.'Analysed_platform1_pileup_Tspec.txt';
    $generic_inputFile_platform2 = $temp_output_directory.'Prepared_platform2.txt';
  
  }elsif($type_of_analysis2 eq "classify"){
  
    # Test match between N and T for somatic
    Lib_SNooPer::SomVsGermTest(\@$sample_t1, \@$sample_n1);
      
    # Pileup analysis
    foreach my $sample_id(@$sample_t1){ 
      my $inputFile = "Platform1_T_".$sample_id.".pu";
      if($type_of_analysis3 eq "SNP"){
	Lib_SNooPer::PileupSNPAnalysis($input_directory, $inputFile, $temp_output_directory, $sample_id, $bqv_platform1, $mqv_platform1, $nbvar_T, $coverage_filter_T, $indel_filter, $qual_filter, $freqinf, $freqsup, $contamination, \@$selected_attributes); 
      }elsif($type_of_analysis3 eq "Indel"){
	Lib_SNooPer::PileupIndelAnalysis($input_directory, $inputFile, $temp_output_directory, $sample_id, $bqv_platform1, $mqv_platform1, $nbvar_T, $coverage_filter_T, $qual_filter, $contamination, \@$selected_attributes); 
      }
      print "Pileup analysis for $inputFile is completed\n";
    }
    if(defined $path_to_germDB){
      Lib_SNooPer::FiltergermDB($temp_output_directory, $path_to_bedtool, $path_to_germDB);
    }
    if(defined $path_to_blacklist){
      Lib_SNooPer::BlackList($temp_output_directory, $path_to_bedtool, $path_to_blacklist, $type_of_analysis1, $path_to_germDB);
    }
    Lib_SNooPer::Formatting($temp_output_directory, $path_to_blacklist, $path_to_germDB, $type_of_analysis1, $qual_filter, \@$selected_attributes);
    if($coverage_filter_N != 0){
      Lib_SNooPer::GermlineFilter($input_directory, $temp_output_directory, $coverage_filter_N, $covered_filter_N);
    }
    
    # Parsing germline pileup files
    if($type_of_analysis3 eq "SNP"){
      Lib_SNooPer::PileupSNPPartialAnalysis($temp_output_directory, $bqv_platform1, $mqv_platform1, $qual_filter, $nbvar_N); 
    }elsif($type_of_analysis3 eq "Indel"){
      Lib_SNooPer::PileupIndelPartialAnalysis($temp_output_directory, $bqv_platform1, $mqv_platform1, $qual_filter, $nbvar_N); 
    }
    print "Germline pileup analysis is completed\n";
        
    # Somatic-specificity analysis
    Lib_SNooPer::SomaticSpecificAnalysis($temp_output_directory, $somatic_pvalue, $covered_filter_N);
    $generic_inputFile_platform1 = $temp_output_directory.'Analysed_platform1_pileup_Tspec.txt';
  }
    
}elsif($type_of_analysis1 eq "germline"){
  
  if($type_of_analysis2 eq "train" || $type_of_analysis2 eq "evaluate"){
    
    # Test intersection between platform1 and platform2
    my($intersect) =  Lib_SNooPer::InterSectTest(\@$sample_1, \@$sample_2);
      
    # Pileup analysis
    foreach my $sample_id(@$sample_1){ 
      my $inputFile = "Platform1_".$sample_id.".pu";
      if($type_of_analysis3 eq "SNP"){
	Lib_SNooPer::PileupSNPAnalysis($input_directory, $inputFile, $temp_output_directory, $sample_id, $bqv_platform1, $mqv_platform1, $nbvar_N, $coverage_filter_N, $indel_filter, $qual_filter, $freqinf, $freqsup, $contamination, \@$selected_attributes); 
      }elsif($type_of_analysis3 eq "Indel"){
	Lib_SNooPer::PileupIndelAnalysis($input_directory, $inputFile, $temp_output_directory, $sample_id, $bqv_platform1, $mqv_platform1, $nbvar_N, $coverage_filter_N, $qual_filter, $contamination, \@$selected_attributes); 
      }
      print "Pileup analysis for $inputFile is completed\n";
    }
    if(defined $path_to_blacklist){
      Lib_SNooPer::BlackList($temp_output_directory, $path_to_bedtool, $path_to_blacklist, $type_of_analysis1, $path_to_germDB);
    }
    Lib_SNooPer::Formatting($temp_output_directory, $path_to_blacklist, $path_to_germDB, $type_of_analysis1, $qual_filter, \@$selected_attributes);
    
    # Parsing platform2 vcf files 
    foreach my $sample_id(@$intersect){ 
      my $inputFile = "Platform2_".$sample_id.".vcf";
      Lib_SNooPer::Plat2parsing($input_directory, $inputFile, $temp_output_directory, $sample_id, $type_of_analysis3); 
      print "Data from $inputFile is parsed\n";
    }
    $generic_inputFile_platform1 = $temp_output_directory.'Analysed_platform1_pileup_4.txt';
    $generic_inputFile_platform2 = $temp_output_directory.'Prepared_platform2.txt';
  
  }elsif($type_of_analysis2 eq "classify"){
    
    # Pileup analysis
    foreach my $sample_id(@$sample_1){ 
      my $inputFile = "Platform1_".$sample_id.".pu";
      if($type_of_analysis3 eq "SNP"){
	Lib_SNooPer::PileupSNPAnalysis($input_directory, $inputFile, $temp_output_directory, $sample_id, $bqv_platform1, $mqv_platform1, $nbvar_N, $coverage_filter_N, $indel_filter, $qual_filter, $freqinf, $freqsup, $contamination, \@$selected_attributes); 
      }elsif($type_of_analysis3 eq "Indel"){
	Lib_SNooPer::PileupIndelAnalysis($input_directory, $inputFile, $temp_output_directory, $sample_id, $bqv_platform1, $mqv_platform1, $nbvar_N, $coverage_filter_N, $qual_filter, $contamination, \@$selected_attributes); 
      }
      print "Pileup analysis for $inputFile is completed\n";
    }
    if(defined $path_to_blacklist){
      Lib_SNooPer::BlackList($temp_output_directory, $path_to_bedtool, $path_to_blacklist, $type_of_analysis1, $path_to_germDB);
    }
    Lib_SNooPer::Formatting($temp_output_directory, $path_to_blacklist, $path_to_germDB, $type_of_analysis1, $qual_filter, \@$selected_attributes);
    $generic_inputFile_platform1 = $temp_output_directory.'Analysed_platform1_pileup_4.txt';
  
  }
  
} 


#====================================================
#                 .arff creation
#==================================================== 

if($type_of_analysis2 eq "train"){
  
  # Training and evaluation .arff creation
  Lib_SNooPer::PrintArffHeadersInFile($outputFile_platform1_train_arff, \@$selected_attributes);
  Lib_SNooPer::ArffCreationTrain($generic_inputFile_platform1, $generic_inputFile_platform2, $outputFile_platform1_train_arff, $nbvalidated, $nbnonvalidated, $validated_variant_fraction, $validated_nonvalidated_ratio);
  
  if (defined $attributes_selection){
  
    if ($attributes_selection eq "MI"){
      # Attributes selection 
      `java $memory -classpath $path_to_weka weka.attributeSelection.InfoGainAttributeEval -s "weka.attributeSelection.Ranker -T 0.001" -i $outputFile_platform1_train_arff > $path_to_attributes`;
      `java $memory -classpath $path_to_weka weka.filters.supervised.attribute.AttributeSelection -E "weka.attributeSelection.InfoGainAttributeEval" -S "weka.attributeSelection.Ranker -T 0.001"  -i $outputFile_platform1_train_arff -o $outputFile_platform1_train_filtered_arff`;
    }elsif ($attributes_selection eq "BestFirst"){
      # Attributes selection
      `java $memory -classpath $path_to_weka weka.attributeSelection.CfsSubsetEval -s "weka.attributeSelection.BestFirst" -i $outputFile_platform1_train_arff > $path_to_attributes`;
      `java $memory -classpath $path_to_weka weka.filters.supervised.attribute.AttributeSelection -E "weka.attributeSelection.CfsSubsetEval" -S "weka.attributeSelection.BestFirst" -i $outputFile_platform1_train_arff -o $outputFile_platform1_train_filtered_arff`;
    }
    
    open (ATT_OUTPUT, ">>$path_to_attributes") or die "Cannot open file $path_to_attributes: $!";
    print (ATT_OUTPUT "\nQuality filter: $qual_filter\nAttributes selection: $attributes_selection");
    close ATT_OUTPUT;
    `rm $outputFile_platform1_train_arff`;
    $outputFile_platform1_train_arff = $outputFile_platform1_train_filtered_arff;

  }

}elsif($type_of_analysis2 eq "evaluate"){
  
  # Test .arff creation
  Lib_SNooPer::PrintArffHeadersInFile($outputFile_platform1_test_arff, \@$selected_attributes);
  Lib_SNooPer::ArffCreationEval($temp_output_directory, $generic_inputFile_platform1, $generic_inputFile_platform2, $outputFile_platform1_test_arff);
  
}elsif($type_of_analysis2 eq "classify"){
  
  # Test .arff creation
  Lib_SNooPer::PrintArffHeadersInFile($outputFile_platform1_test_arff, \@$selected_attributes);
  Lib_SNooPer::ArffCreation($generic_inputFile_platform1, $outputFile_platform1_test_arff);
  
}
  
  
#====================================================
#               Algorithm training
#====================================================  
  
if($type_of_analysis2 eq "train"){
  
  if (! defined $cost_matrix){
    
    # Saving model
    `java $memory -classpath $path_to_weka weka.classifiers.$algorithm -I $tree -S 1 -t $outputFile_platform1_train_arff -x $cross_validation -v -o -d $path_to_model > $training_stats`;
    # Stat curve coordinates
    `java -classpath $path_to_weka weka.classifiers.$algorithm -I $tree -S 1 -t $outputFile_platform1_train_arff -threshold-file $stat_curve -threshold-label 1`;
    
  }else{
    
    # Saving model
    `java $memory -classpath $path_to_weka weka.classifiers.meta.CostSensitiveClassifier -t $outputFile_platform1_train_arff -d $path_to_model -cost-matrix "$cost_matrix" -W weka.classifiers.$algorithm -- -I $tree -S 1 > $training_stats`;
    # Stat curve coordinates
    `java -classpath $path_to_weka weka.classifiers.meta.CostSensitiveClassifier -t $outputFile_platform1_train_arff -cost-matrix "$cost_matrix" -threshold-file $stat_curve -threshold-label 1 -W weka.classifiers.$algorithm -- -I $tree -S 1`;

  }
  
  # Construction of ROC and Precision Recal curves 
  Lib_SNooPer::CurvesCreation($stat_curve, $temp_output_directory, $output_directory);
   
}


#====================================================
#                 Classification 
#====================================================  

if($type_of_analysis2 eq "classify" || $type_of_analysis2 eq "evaluate"){
        
  # Algorithm applying to the complete dataset
  `java $memory -classpath $path_to_weka weka.classifiers.$algorithm -T $outputFile_platform1_test_arff -l $path_to_model -p 0 > $test_raw_classification`;
     
  # Final final construction
  Lib_SNooPer::FinalFileCreation($final_classification, $generic_inputFile_platform1, $test_raw_classification, $type_of_analysis1, $type_of_analysis3); 
  print "The final classification has been completed with success\n";

}

if($type_of_analysis2 eq "evaluate"){
 
  # Evaluation of the algorithm
  Lib_SNooPer::EvalStat($test_raw_classification, $eval_classification_stat);
  print "The evaluation has been completed with success\n";
  
}


#====================================================
#              Temp directory cleaning 
#====================================================

# Temp files suppression
Lib_SNooPer::TempDirClean($temp_output_directory);


exit 0;
